package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Competicion;
import clasesContenedoras.Partido;
import interfacesImplementacion.Persistencia;

public class CRUDCompeticiones extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tabla;
	private Persistencia bd;
	private JTextField nombreCompeticion;
	private JCheckBox chckbxEliminar;
	private List<Competicion> competiciones;
	private JButton btnCancelar;
	private JTextField textFieldFechaInicio;
	private JTextField textFieldFechaFin;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public CRUDCompeticiones(Persistencia bd) {
		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		try {
			competiciones = (List<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Competicion", e1,
					"Se ha producido un erro al listar la tabla 'Competicion'");
			e1.printStackTrace();
			return;
		}

		setBounds(100, 100, 534, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 165, 491, 105);
		contentPane.add(scrollPane);

		tabla = new JTable();
		scrollPane.setViewportView(tabla);

		mostrarTabla();

		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				cambiarNombreSeleccionado();
			}
		});

		JLabel lblNombreDelEquipo = new JLabel("Nombre de la competición");
		lblNombreDelEquipo.setBounds(12, 42, 151, 15);
		contentPane.add(lblNombreDelEquipo);

		nombreCompeticion = new JTextField();
		nombreCompeticion.setBounds(171, 40, 332, 19);
		contentPane.add(nombreCompeticion);
		nombreCompeticion.setColumns(10);

		JButton btnGuardar = new JButton("Aceptar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonAceptar();
			}
		});
		btnGuardar.setBounds(386, 129, 117, 25);
		contentPane.add(btnGuardar);

		chckbxEliminar = new JCheckBox("Eliminar");
		chckbxEliminar.setBounds(12, 130, 104, 23);
		contentPane.add(chckbxEliminar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonCancelar();
			}
		});
		btnCancelar.setBounds(261, 129, 117, 25);
		contentPane.add(btnCancelar);

		JLabel lblFechaInicio = new JLabel("Fecha Inicio");
		lblFechaInicio.setBounds(12, 73, 131, 14);
		contentPane.add(lblFechaInicio);

		textFieldFechaInicio = new JTextField();
		textFieldFechaInicio.setBounds(171, 70, 123, 20);
		contentPane.add(textFieldFechaInicio);
		textFieldFechaInicio.setColumns(10);

		JLabel lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(12, 104, 117, 14);
		contentPane.add(lblFechaFin);

		textFieldFechaFin = new JTextField();
		textFieldFechaFin.setBounds(171, 101, 123, 20);
		contentPane.add(textFieldFechaFin);
		textFieldFechaFin.setColumns(10);

		JLabel lblddmmaaaa = new JLabel("(aaaa-mm-dd)");
		lblddmmaaaa.setBounds(304, 76, 129, 14);
		contentPane.add(lblddmmaaaa);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int posicion = competiciones.lastIndexOf(new Competicion(nombreCompeticion.getText(), null, null));

				if (posicion != -1) {
					tabla.setRowSelectionInterval(posicion, posicion);
				} else {
					Utiles.notificaError(null, "Equipo no encontrado", null,
							"No se ha encontrado el equipo: " + nombreCompeticion.getText());
				}
			}
		});
		btnBuscar.setBounds(134, 129, 117, 25);
		contentPane.add(btnBuscar);

		JLabel label = new JLabel("(aaaa-mm-dd)");
		label.setBounds(304, 104, 129, 14);
		contentPane.add(label);

	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			dispose();

		}
	}

	protected void cambiarNombreSeleccionado() {
		if (tabla.getSelectedRow() != -1) {
			nombreCompeticion.setText((String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 1));
			textFieldFechaInicio.setText((String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 2));
			textFieldFechaFin.setText((String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 3));
		}

	}

	protected void botonCancelar() {
		tabla.clearSelection();
		nombreCompeticion.setText("");
		textFieldFechaInicio.setText("");
		textFieldFechaFin.setText("");
		nombreCompeticion.grabFocus();
	}

	@SuppressWarnings({ "serial", "unchecked" })
	private void mostrarTabla() {

		competiciones.clear();

		try {
			competiciones = (List<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Competicion", e1,
					"Se ha producido un erro al listar la tabla 'Competicion'");
			return;
		}

		String[] tituloColumna = { "ID", "Nombre Competición", "Fecha Inicio", "Fecha Fin" };

		try {
			tabla.setModel(new DefaultTableModel(equiposAMatriz(competiciones), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}

			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Competicion", ex,
					"Se ha producido un erro al listar la tabla 'Competicion'");
			return;
		}

		tabla.grabFocus();
		if (tabla.getRowCount() != 0)
			tabla.setRowSelectionInterval(0, 0);
		tabla.setSelectionMode(0);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.getColumnModel().getColumn(0).setPreferredWidth(35);
		tabla.getColumnModel().getColumn(1).setMinWidth(253);
		tabla.getColumnModel().getColumn(2).setMinWidth(100);
		tabla.getColumnModel().getColumn(3).setMinWidth(100);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

	}

	private String[][] equiposAMatriz(List<Competicion> competiciones) {
		String[][] resultado = new String[competiciones.size()][4];

		for (int i = 0; i < competiciones.size(); i++) {
			resultado[i][0] = String.valueOf(competiciones.get(i).getId());
			resultado[i][1] = String.valueOf(competiciones.get(i).getNombre());

			String fechaInicio = Utiles.formatearDateToString(competiciones.get(i).getFechaComienzo());
			String fechaFin = Utiles.formatearDateToString(competiciones.get(i).getFechaFin());

			resultado[i][2] = fechaInicio;
			resultado[i][3] = fechaFin;
		}

		return resultado;
	}

	private void botonAceptar() {

		if (chckbxEliminar.isSelected()) {

			borrar();

		} else {
			if (!nombreCompeticion.getText().isEmpty()) {

				if (tabla.getSelectedRow() == -1) {
					guardar();
				} else {

					String stringFechaInicio = textFieldFechaInicio.getText();
					String stringFechaFinal = textFieldFechaFin.getText();

					if (!stringFechaInicio.matches(".{4}[-].{2}[-].{2}")
							|| !stringFechaFinal.matches(".{4}[-].{2}[-].{2}")) {
						Utiles.notificaError(null, "Error con las fechas", null,
								"El formato de las fechas no es correcto");
						return;
					}

					modificar();
				}

			} else {
				Utiles.notificaError(null, "Nombre vacío", null, "Introduzca un nombre para dar de alta");
			}

		}

		botonCancelar();

	}

	private void modificar() {

		Competicion competiticionSeleccionada = competiciones.get(tabla.getSelectedRow());

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de modificar la competicion: '" + competiticionSeleccionada.getNombre() + "'?",
				"Modificar competicion", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			Date fechaInicio = null;
			Date fechaFin = null;

			String stringFechaInicio = textFieldFechaInicio.getText();
			String stringFechaFinal = textFieldFechaFin.getText();

			try {
				try {
					fechaInicio = Utiles.formatearStringToDate(stringFechaInicio);
					fechaFin = Utiles.formatearStringToDate(stringFechaFinal);

					if (fechaInicio.getTime() > fechaFin.getTime()) {
						Utiles.notificaError(null, "Error con las fechas", null,
								"La fecha de inicio es superior a la de fin");
						return;
					}

				} catch (ParseException pe) {
					Utiles.notificaError(null, "Error con las fechas", null, "El formato de las fechas no es correcto");
					return;
				}
				competiticionSeleccionada.setNombre(nombreCompeticion.getText());

				competiticionSeleccionada.setFechaComienzo(fechaInicio);

				competiticionSeleccionada.setFechaFin(fechaFin);

				bd.modificarDatosByID(competiticionSeleccionada);
				mostrarTabla();
			} catch (Exception e) {
				Utiles.notificaError(null, "Error al modificar", e, "Se ha producido un error al modificar");
				e.printStackTrace();
			}
		}

	}

	private void guardar() {

		try {

			Date fechaInicio = Utiles.formatearStringToDate(textFieldFechaInicio.getText().trim());
			Date fechaFin = Utiles.formatearStringToDate(textFieldFechaFin.getText().trim());

			if (fechaInicio.getTime() > fechaFin.getTime()) {
				Utiles.notificaError(null, "Error con las fechas", null, "La fecha de inicio es superior a la de fin");
				return;
			}

			Competicion nuevoEquipo = new Competicion(nombreCompeticion.getText(), fechaInicio, fechaFin);

			if (competiciones.contains(nuevoEquipo)) {
				Utiles.notificaError(null, "Equipo ya existe", null, "El equipo ya existe");
				return;
			}

			bd.guardarDatos(nuevoEquipo);
			competiciones.add(nuevoEquipo);
			mostrarTabla();
			tabla.revalidate();
			tabla.repaint();

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al guardar", e1,
					"Se ha producido un error al guardar el equipo en la base de datos");

		}

	}

	@SuppressWarnings("unchecked")
	private void borrar() {
		if (tabla.getSelectedRow() == -1) {
			Utiles.notificaError(null, "Error al eliminar", null, "Para eliminar seleccione un equipo en la tabla");
			return;
		}

		Competicion competicionSeleccionada = competiciones.get(tabla.getSelectedRow());

		try {
			ArrayList<Partido> partidos = (ArrayList<Partido>) bd.consultarDatosByObjetos(Partido.class, true,
					competicionSeleccionada);

			if (!partidos.isEmpty()) {
				Utiles.notificaError(null, "No se puede borrar", null,
						"Hay partidos en la competicion que desea borrar, elimine los partidos asociados");
				return;
			}

		} catch (Exception e) {
			Utiles.notificaError(null, "Error al borrar", e,
					"Se ha producido un error al comprobar la competicion en la base de datos");
			return;
		}

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de eliminar el equipo: '" + competicionSeleccionada.getNombre() + "'?",
				"Eliminar Competicion", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				bd.borrarDatos(competiciones.get(tabla.getSelectedRow()));
				mostrarTabla();
			} catch (Exception e1) {
				Utiles.notificaError(null, "Error al borrar", e1,
						"Se ha producido un error al borrar la competicion en la base de datos");
			}
		}

	}
}
