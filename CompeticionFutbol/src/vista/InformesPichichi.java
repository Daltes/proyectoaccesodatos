package vista;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Competicion;
import clasesContenedoras.Estadistica;
import clasesContenedoras.Partido;
import interfacesImplementacion.Persistencia;
import javax.swing.JComboBox;

public class InformesPichichi extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tabla;
	private Persistencia bd;
	private List<Partido> partidos;
	private ArrayList<Competicion> listaCompeticiones;
	private JComboBox comboBoxCompeticiones;
	private Map<String, Integer> listaClasificacion;

	/**
	 * Create the frame.
	 */
	public InformesPichichi(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		listaClasificacion = new TreeMap<>();

		setBounds(100, 100, 524, 468);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 64, 492, 354);
		contentPane.add(scrollPane);

		tabla = new JTable();
		scrollPane.setViewportView(tabla);

		JLabel lblCompeticin = new JLabel("Competición");
		lblCompeticin.setBounds(6, 30, 108, 14);
		contentPane.add(lblCompeticin);

		comboBoxCompeticiones = new JComboBox();
		comboBoxCompeticiones.setBounds(137, 28, 198, 18);
		contentPane.add(comboBoxCompeticiones);

		comboBoxCompeticiones.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					filtrarPartidos();
				}
			}
		});

		rellenarComboBoxCompeticiones();
	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxCompeticiones() {

		try {
			listaCompeticiones = (ArrayList<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar las competiciones");
			return;
		}

		for (Competicion competicion : listaCompeticiones) {
			comboBoxCompeticiones.addItem(competicion.getNombre());
		}
	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			this.dispose();

		}
	}

	@SuppressWarnings({ "serial" })
	private void mostrarTabla(LinkedHashMap<String, Integer> resultado) {

		String[] tituloColumna = { "Posicion", "Nombre Jugador", "Goles" };

		try {
			tabla.setModel(new DefaultTableModel(equiposAMatriz(resultado), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

	}

	private String[][] equiposAMatriz(LinkedHashMap<String, Integer> resultado2) {
		String[][] resultado = new String[resultado2.size()][3];
		ArrayList<String> claves = new ArrayList<String>(resultado2.keySet());
		int j = 0;
		for (int i = claves.size() - 1; i >= 0; i--) {
			// for (int i = 0; i < claves.size(); i++) {
			resultado[j][0] = String.valueOf(i + 1);
			resultado[j][1] = claves.get(i);
			resultado[j][2] = resultado2.get(claves.get(i)).toString();
			j++;
		}

		return resultado;
	}

	@SuppressWarnings("unchecked")
	private void filtrarPartidos() {
		try {
			partidos = (ArrayList<Partido>) bd.consultarDatosByObjetos(Partido.class, false,
					(Competicion) listaCompeticiones.get(comboBoxCompeticiones.getSelectedIndex()));
		} catch (Exception e1) {
			Utiles.notificaError(null, "Error a listar", e1, "Se ha producido un error al listar los partidos");
			return;
		}

		listaClasificacion.clear();

		for (Partido partido : partidos) {
			ArrayList<Estadistica> estadisticas;
			try {
				estadisticas = (ArrayList<Estadistica>) bd.consultarDatosByObjetos(Estadistica.class, true, partido);
			} catch (Exception e) {
				Utiles.notificaError(null, "Error a listar", e, "Se ha producido un error al listar las estadisticas");
				return;
			}

			for (Estadistica estadistica : estadisticas) {

				int nuevos = 0;

				if (listaClasificacion.containsKey(estadistica.getJugador().getNombre())) {
					int valorActual = listaClasificacion.get(estadistica.getJugador().getNombre());
					nuevos = estadistica.getGoles();
					valorActual += nuevos;

					listaClasificacion.put(estadistica.getJugador().getNombre(), valorActual);

				} else {
					nuevos = estadistica.getGoles();
					listaClasificacion.put(estadistica.getJugador().getNombre(), nuevos);
				}

			}

		}

		LinkedHashMap<String, Integer> resultado = (LinkedHashMap<String, Integer>) Utiles
				.sortByValue(listaClasificacion);

		/*
		 * Map<String, Integer> prueba = new TreeMap<>(); prueba.put("tercero", 10);
		 * prueba.put("Primero", 1); prueba.put("Segundo", 5);
		 * 
		 * TreeMap<String, Integer> resultadoPrueba = (TreeMap<String, Integer>)
		 * Utiles.sortByValue(prueba); ArrayList<String> claves = new
		 * ArrayList<String>(resultadoPrueba.keySet()); for(int i=claves.size()-1;
		 * i>=0;i--){ System.out.println(resultadoPrueba.get(claves.get(i))); }
		 */

		// System.out.println(resultadoPrueba);

		// System.out.println(resultado);

		mostrarTabla(resultado);
	}
}
