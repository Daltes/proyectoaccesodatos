package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.util.Util;

import clasesContenedoras.Competicion;
import clasesContenedoras.Equipo;
import clasesContenedoras.Estadistica;
import clasesContenedoras.Jugador;
import clasesContenedoras.Partido;
import clasesContenedoras.Posicion;
import interfacesImplementacion.Persistencia;
import jdk.nashorn.internal.runtime.ListAdapter;

import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.JRadioButton;
import javax.swing.DefaultComboBoxModel;

public class CompeticionGestionarPartidos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tabla;
	private Persistencia bd;
	private JTextField textFieldID;
	private JCheckBox chckbxEliminar;
	private List<Partido> partidos;
	private ArrayList<Competicion> listaCompeticiones;
	private ArrayList<Equipo> listaEquipos;
	private JComboBox comboBoxCompeticiones;
	private JTextField textFieldFecha;
	private JComboBox comboBoxListaEquipoLocal;
	private JComboBox comboBoxListaEquipoVisitante;
	private JTextField textFieldGolesLocal;
	private JTextField textFieldGolesVisitantes;
	private JComboBox comboBoxJugadoresLocal;
	private JComboBox comboBoxJugadoresVisitante;
	private ArrayList<Jugador> listaJugadoresLocal;
	private ArrayList<Jugador> listaJugadoresVisitante;
	private JTextField textFieldJugadorGolesLocal;
	private JTextField textFieldJugadorGolesVisitante;
	private JTextField textFieldJugadorFaltasLocal;
	private JTextField textFieldJugadorFaltasVisitante;
	private JRadioButton rdbtnTieneTarjetaRojaLocal;
	private JRadioButton rdbtnTieneTarjetaRojaVisitante;
	private JComboBox comboBoxTarjetasAmarillasLocal;
	private JComboBox comboBoxTarjetasAmarillasVisitante;

	/**
	 * Create the frame.
	 */
	public CompeticionGestionarPartidos(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		listaEquipos = new ArrayList<>();
		listaJugadoresLocal = new ArrayList<>();
		listaJugadoresVisitante = new ArrayList<>();

		setBounds(100, 100, 1027, 799);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 317, 989, 105);
		contentPane.add(scrollPane);

		tabla = new JTable();
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tabla);

		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				cambiarPartidoSeleccionado();
			}
		});

		JLabel lblID = new JLabel("ID");
		lblID.setBounds(12, 42, 55, 15);
		contentPane.add(lblID);

		textFieldID = new JTextField();
		textFieldID.setBounds(216, 42, 78, 19);
		contentPane.add(textFieldID);
		textFieldID.setColumns(10);

		JButton btnGuardar = new JButton("Aceptar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonAceptar();
			}
		});
		btnGuardar.setBounds(380, 266, 117, 25);
		contentPane.add(btnGuardar);

		chckbxEliminar = new JCheckBox("Eliminar");
		chckbxEliminar.setBounds(6, 272, 108, 23);
		contentPane.add(chckbxEliminar);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscar();
			}
		});
		btnBuscar.setBounds(126, 266, 117, 25);
		contentPane.add(btnBuscar);

		JLabel lblCompeticin = new JLabel("Competición");
		lblCompeticin.setBounds(521, 42, 108, 14);
		contentPane.add(lblCompeticin);

		comboBoxCompeticiones = new JComboBox();
		comboBoxCompeticiones.setBounds(639, 39, 198, 18);
		contentPane.add(comboBoxCompeticiones);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonCancelar();
			}
		});
		btnCancelar.setBounds(253, 267, 117, 25);
		contentPane.add(btnCancelar);

		JLabel lblFecha = new JLabel("Fecha( aaaa-mm-dd hh:mm:ss )");
		lblFecha.setBounds(10, 104, 196, 14);
		contentPane.add(lblFecha);

		textFieldFecha = new JTextField();
		textFieldFecha.setBounds(216, 104, 251, 20);
		contentPane.add(textFieldFecha);
		textFieldFecha.setColumns(10);

		JLabel lblEquipoLocal = new JLabel("Equipo Local");
		lblEquipoLocal.setBounds(12, 158, 78, 14);
		contentPane.add(lblEquipoLocal);

		comboBoxListaEquipoLocal = new JComboBox();
		comboBoxListaEquipoLocal.setBounds(216, 158, 204, 17);
		contentPane.add(comboBoxListaEquipoLocal);

		JLabel lblEquipoVisitante = new JLabel("Equipo Visitante");
		lblEquipoVisitante.setBounds(521, 158, 102, 14);
		contentPane.add(lblEquipoVisitante);

		comboBoxListaEquipoVisitante = new JComboBox();
		comboBoxListaEquipoVisitante.setBounds(639, 155, 198, 17);
		contentPane.add(comboBoxListaEquipoVisitante);

		JLabel lblGolesLocal = new JLabel("Goles Local");
		lblGolesLocal.setBounds(12, 216, 146, 14);
		contentPane.add(lblGolesLocal);

		JLabel lblGolesVisitante = new JLabel("Goles Visitante");
		lblGolesVisitante.setBounds(521, 216, 108, 14);
		contentPane.add(lblGolesVisitante);

		textFieldGolesLocal = new JTextField();
		textFieldGolesLocal.setBounds(216, 216, 204, 20);
		contentPane.add(textFieldGolesLocal);
		textFieldGolesLocal.setColumns(10);

		textFieldGolesVisitantes = new JTextField();
		textFieldGolesVisitantes.setBounds(639, 213, 198, 20);
		contentPane.add(textFieldGolesVisitantes);
		textFieldGolesVisitantes.setColumns(10);

		JLabel lblNewLabel = new JLabel("Si se da de alta no se seleccionará");
		lblNewLabel.setBounds(304, 45, 217, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel(" el ID indicado");
		lblNewLabel_1.setBounds(294, 56, 163, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblLocal = new JLabel("Local");
		lblLocal.setBounds(257, 451, 67, 14);
		contentPane.add(lblLocal);

		JLabel lblVisitante = new JLabel("Visitante");
		lblVisitante.setBounds(749, 451, 86, 14);
		contentPane.add(lblVisitante);

		JLabel lblJugador = new JLabel("Jugador");
		lblJugador.setBounds(6, 486, 84, 14);
		contentPane.add(lblJugador);

		JLabel label = new JLabel("Jugador");
		label.setBounds(539, 486, 90, 14);
		contentPane.add(label);

		comboBoxJugadoresLocal = new JComboBox();
		comboBoxJugadoresLocal.setBounds(136, 481, 258, 19);
		contentPane.add(comboBoxJugadoresLocal);

		comboBoxJugadoresVisitante = new JComboBox();
		comboBoxJugadoresVisitante.setBounds(656, 481, 258, 19);
		contentPane.add(comboBoxJugadoresVisitante);

		JLabel lblGoles = new JLabel("Goles");
		lblGoles.setBounds(6, 540, 108, 14);
		contentPane.add(lblGoles);

		JLabel lblFaltas = new JLabel("Faltas");
		lblFaltas.setBounds(6, 576, 108, 14);
		contentPane.add(lblFaltas);

		JLabel lblTarjetasAmarillas = new JLabel("Tarjetas Amarillas");
		lblTarjetasAmarillas.setBounds(6, 610, 108, 14);
		contentPane.add(lblTarjetasAmarillas);

		JLabel lblTarjetaRoja = new JLabel("Tarjeta Roja");
		lblTarjetaRoja.setBounds(6, 646, 84, 14);
		contentPane.add(lblTarjetaRoja);

		JLabel label_1 = new JLabel("Tarjeta Roja");
		label_1.setBounds(539, 646, 84, 14);
		contentPane.add(label_1);

		JLabel label_2 = new JLabel("Tarjetas Amarillas");
		label_2.setBounds(539, 610, 108, 14);
		contentPane.add(label_2);

		JLabel label_3 = new JLabel("Faltas");
		label_3.setBounds(539, 576, 108, 14);
		contentPane.add(label_3);

		JLabel label_4 = new JLabel("Goles");
		label_4.setBounds(539, 540, 108, 14);
		contentPane.add(label_4);

		textFieldJugadorGolesLocal = new JTextField();
		textFieldJugadorGolesLocal.setBounds(136, 537, 86, 20);
		contentPane.add(textFieldJugadorGolesLocal);
		textFieldJugadorGolesLocal.setColumns(10);

		textFieldJugadorGolesVisitante = new JTextField();
		textFieldJugadorGolesVisitante.setColumns(10);
		textFieldJugadorGolesVisitante.setBounds(656, 537, 86, 20);
		contentPane.add(textFieldJugadorGolesVisitante);

		textFieldJugadorFaltasLocal = new JTextField();
		textFieldJugadorFaltasLocal.setBounds(136, 573, 86, 20);
		contentPane.add(textFieldJugadorFaltasLocal);
		textFieldJugadorFaltasLocal.setColumns(10);

		textFieldJugadorFaltasVisitante = new JTextField();
		textFieldJugadorFaltasVisitante.setColumns(10);
		textFieldJugadorFaltasVisitante.setBounds(656, 573, 86, 20);
		contentPane.add(textFieldJugadorFaltasVisitante);

		JButton btnAceptarJugadorLocal = new JButton("Aceptar");
		btnAceptarJugadorLocal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estadisticaGuardar(true);
			}
		});
		btnAceptarJugadorLocal.setBounds(6, 678, 89, 23);
		contentPane.add(btnAceptarJugadorLocal);

		JButton btnAceptarJugadorVisitante = new JButton("Aceptar");
		btnAceptarJugadorVisitante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				estadisticaGuardar(false);
			}
		});
		btnAceptarJugadorVisitante.setBounds(825, 678, 89, 23);
		contentPane.add(btnAceptarJugadorVisitante);

		rdbtnTieneTarjetaRojaLocal = new JRadioButton("Tiene tarjeta roja");
		rdbtnTieneTarjetaRojaLocal.setBounds(136, 642, 158, 23);
		contentPane.add(rdbtnTieneTarjetaRojaLocal);

		rdbtnTieneTarjetaRojaVisitante = new JRadioButton("Tiene tarjeta roja");
		rdbtnTieneTarjetaRojaVisitante.setBounds(656, 642, 163, 23);
		contentPane.add(rdbtnTieneTarjetaRojaVisitante);

		comboBoxTarjetasAmarillasLocal = new JComboBox();
		comboBoxTarjetasAmarillasLocal.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2" }));
		comboBoxTarjetasAmarillasLocal.setBounds(136, 607, 86, 19);
		contentPane.add(comboBoxTarjetasAmarillasLocal);

		comboBoxTarjetasAmarillasVisitante = new JComboBox();
		comboBoxTarjetasAmarillasVisitante.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2" }));
		comboBoxTarjetasAmarillasVisitante.setBounds(656, 607, 86, 19);
		contentPane.add(comboBoxTarjetasAmarillasVisitante);

		comboBoxCompeticiones.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					filtrarPartidos();
				}
			}
		});

		comboBoxJugadoresLocal.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					pintarEstadistica(true);
				}
			}
		});

		comboBoxJugadoresVisitante.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					pintarEstadistica(false);
				}
			}
		});

		rellenarComboBoxCompeticiones();
	}

	protected void pintarEstadistica(boolean esLocal) {

		if (tabla.getSelectedRow() == -1) {
			return;
		}

		Jugador jugador = null;
		if (esLocal) {
			if (comboBoxJugadoresLocal.getSelectedIndex() == -1) {
				return;
			}

			if (listaJugadoresLocal.isEmpty())
				return;
			jugador = listaJugadoresLocal.get(comboBoxJugadoresLocal.getSelectedIndex());
		} else {
			if (comboBoxJugadoresVisitante.getSelectedIndex() == -1) {
				return;
			}

			if (listaJugadoresVisitante.isEmpty())
				return;
			jugador = listaJugadoresVisitante.get(comboBoxJugadoresVisitante.getSelectedIndex());
		}

		Partido partido = partidos.get(tabla.getSelectedRow());

		String licencia = String.valueOf(jugador.getLicencia());
		String idPartido = String.valueOf(partido.getId());

		Estadistica estadistica = null;
		try {
			estadistica = (Estadistica) bd.consultarDatoByID(Estadistica.class, idPartido, licencia);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al consultar", e,
					"Se ha producido un error al consultar las estadisticas en la base de datos");
			return;
		}

		if (estadistica == null) {

			if (esLocal) {
				textFieldJugadorFaltasLocal.setText("");
				textFieldJugadorGolesLocal.setText("");
				rdbtnTieneTarjetaRojaLocal.setSelected(false);
				comboBoxTarjetasAmarillasLocal.setSelectedIndex(0);
			} else {
				textFieldJugadorFaltasVisitante.setText("");
				textFieldJugadorGolesVisitante.setText("");
				rdbtnTieneTarjetaRojaVisitante.setSelected(false);
				comboBoxTarjetasAmarillasVisitante.setSelectedIndex(0);
			}

			return;
		}

		String goles = String.valueOf(estadistica.getGoles());
		String faltas = String.valueOf(estadistica.getFaltas());
		boolean tarjetaRoja = estadistica.getTarjRojas() == 1;
		int tarjetasAmarillas = estadistica.getTarjAmarillas();

		if (esLocal) {
			textFieldJugadorFaltasLocal.setText(faltas);
			textFieldJugadorGolesLocal.setText(goles);
			rdbtnTieneTarjetaRojaLocal.setSelected(tarjetaRoja);
			comboBoxTarjetasAmarillasLocal.setSelectedIndex(tarjetasAmarillas);
		} else {
			textFieldJugadorFaltasVisitante.setText(faltas);
			textFieldJugadorGolesVisitante.setText(goles);
			rdbtnTieneTarjetaRojaVisitante.setSelected(tarjetaRoja);
			comboBoxTarjetasAmarillasVisitante.setSelectedIndex(tarjetasAmarillas);
		}

	}

	protected void estadisticaGuardar(boolean esLocal) {

		if (tabla.getSelectedRow() == -1) {
			return;
		}

		Jugador jugador = null;
		if (esLocal) {
			if (comboBoxJugadoresLocal.getSelectedIndex() == -1) {
				return;
			}

			if (listaJugadoresLocal.isEmpty())
				return;
			jugador = listaJugadoresLocal.get(comboBoxJugadoresLocal.getSelectedIndex());
		} else {
			if (comboBoxJugadoresVisitante.getSelectedIndex() == -1) {
				return;
			}

			if (listaJugadoresVisitante.isEmpty())
				return;
			jugador = listaJugadoresVisitante.get(comboBoxJugadoresVisitante.getSelectedIndex());
		}

		Partido partido = partidos.get(tabla.getSelectedRow());

		String licencia = String.valueOf(jugador.getLicencia());
		String idPartido = String.valueOf(partido.getId());

		Estadistica estadistica = null;
		try {
			estadistica = (Estadistica) bd.consultarDatoByID(Estadistica.class, idPartido, licencia);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al consultar", e,
					"Se ha producido un error al consultar las estadisticas en la base de datos");
			return;
		}

		int goles;
		int tarjetasAmarillas;
		int tarjetasRojas;
		int faltas;

		if (esLocal) {

			if (!textFieldJugadorFaltasLocal.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Faltas incorrectas", null, "Introduzca un número correcto de faltas");
				return;
			}

			if (!textFieldJugadorGolesLocal.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Goles incorrectas", null, "Introduzca un número correcto de goles");
				return;
			}

			goles = Integer.parseInt(textFieldJugadorGolesLocal.getText());
			tarjetasAmarillas = comboBoxTarjetasAmarillasLocal.getSelectedIndex();
			tarjetasRojas = rdbtnTieneTarjetaRojaLocal.isSelected() ? 1 : 0;
			faltas = Integer.parseInt(textFieldJugadorFaltasLocal.getText());
		} else {

			if (!textFieldJugadorFaltasVisitante.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Faltas incorrectas", null, "Introduzca un número correcto de faltas");
				return;
			}

			if (!textFieldJugadorGolesVisitante.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Goles incorrectas", null, "Introduzca un número correcto de goles");
				return;
			}

			goles = Integer.parseInt(textFieldJugadorGolesVisitante.getText());
			tarjetasAmarillas = comboBoxTarjetasAmarillasVisitante.getSelectedIndex();
			tarjetasRojas = rdbtnTieneTarjetaRojaVisitante.isSelected() ? 1 : 0;
			faltas = Integer.parseInt(textFieldJugadorFaltasVisitante.getText());
		}

		if (estadistica == null) {
			estadistica = new Estadistica(jugador, partido, goles, faltas, tarjetasAmarillas, tarjetasRojas);

			try {

				// System.out.println(jugador.getLicencia());
				// System.out.println(estadistica.getId());
				bd.guardarDatos(estadistica);
			} catch (Exception e) {
				e.printStackTrace();
				Utiles.notificaError(null, "Error al guardar", e,
						"Se ha producido un error al guardar la estadistica en la base de datos");
				return;
			}
		} else {

			estadistica.setFaltas(faltas);
			estadistica.setGoles(goles);
			estadistica.setTarjAmarillas(tarjetasAmarillas);
			estadistica.setTarjRojas(tarjetasRojas);

			try {

				if (JOptionPane.showConfirmDialog(null, "¿Estás seguro de modificar las estadisticas'?",
						"modificar estadisticas", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {

					bd.modificarDatosByID(estadistica);
				}
			} catch (Exception e) {
				Utiles.notificaError(null, "Error al modificar", e,
						"Se ha producido un error al modificar la estadistica en la base de datos");
				return;
			}

		}

	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxEquipos() {

		comboBoxListaEquipoLocal.removeAllItems();
		comboBoxListaEquipoVisitante.removeAllItems();
		listaEquipos.clear();

		for (Partido partido : partidos) {

			if (!listaEquipos.contains(partido.getEquipoByIdLocal())) {
				listaEquipos.add(partido.getEquipoByIdLocal());
				comboBoxListaEquipoLocal.addItem(partido.getEquipoByIdLocal().getNombre());
				comboBoxListaEquipoVisitante.addItem(partido.getEquipoByIdLocal().getNombre());
			}

			if (!listaEquipos.contains(partido.getEquipoByIdVisitante())) {
				listaEquipos.add(partido.getEquipoByIdVisitante());
				comboBoxListaEquipoLocal.addItem(partido.getEquipoByIdVisitante().getNombre());
				comboBoxListaEquipoVisitante.addItem(partido.getEquipoByIdVisitante().getNombre());
			}

		}

	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxCompeticiones() {

		try {
			listaCompeticiones = (ArrayList<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar las competiciones");
			return;
		}

		for (Competicion competicion : listaCompeticiones) {
			comboBoxCompeticiones.addItem(competicion.getNombre());
		}
	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			this.dispose();

		}
	}

	protected void cambiarPartidoSeleccionado() {
		if (tabla.getSelectedRow() == -1) {
			comboBoxJugadoresLocal.removeAllItems();
			comboBoxJugadoresVisitante.removeAllItems();
			return;
		}

		textFieldID.setEditable(false);

		Partido partidoSeleccionado = partidos.get(tabla.getSelectedRow());

		textFieldID.setText(String.valueOf(partidoSeleccionado.getId()));

		textFieldFecha.setText(Utiles.convertirDateToString(partidoSeleccionado.getFechaHora(), true));

		textFieldGolesLocal.setText(String.valueOf(partidoSeleccionado.getGolesLocal()));
		textFieldGolesVisitantes.setText(String.valueOf(partidoSeleccionado.getGolesVisitante()));

		int posicionLocal = listaEquipos.lastIndexOf(partidoSeleccionado.getEquipoByIdLocal());
		int posicionVisitante = listaEquipos.lastIndexOf(partidoSeleccionado.getEquipoByIdVisitante());

		comboBoxListaEquipoLocal.setSelectedIndex(posicionLocal);
		comboBoxListaEquipoVisitante.setSelectedIndex(posicionVisitante);

		rellenarJugadoresLocal();
		rellenarJugadoresVisitante();

	}

	@SuppressWarnings("unchecked")
	private void rellenarJugadoresVisitante() {
		Equipo equipoSeleccionado = partidos.get(tabla.getSelectedRow()).getEquipoByIdVisitante();
		listaJugadoresVisitante.clear();
		try {
			listaJugadoresVisitante = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class, true,
					equipoSeleccionado);
		} catch (Exception e) {
			Utiles.notificaError(null, "error al listar", e,
					"Se ha producido un error al listar los jugadores locales");
			return;
		}

		comboBoxJugadoresVisitante.removeAllItems();

		for (Jugador jugador : listaJugadoresVisitante) {
			comboBoxJugadoresVisitante.addItem(jugador.getNombre());
		}
	}

	@SuppressWarnings("unchecked")
	private void rellenarJugadoresLocal() {

		Equipo equipoSeleccionado = partidos.get(tabla.getSelectedRow()).getEquipoByIdLocal();
		listaJugadoresLocal.clear();
		try {
			listaJugadoresLocal = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class, true,
					equipoSeleccionado);
		} catch (Exception e) {
			Utiles.notificaError(null, "error al listar", e,
					"Se ha producido un error al listar los jugadores locales");
			return;
		}

		comboBoxJugadoresLocal.removeAllItems();

		for (Jugador jugador : listaJugadoresLocal) {
			comboBoxJugadoresLocal.addItem(jugador.getNombre());
		}

	}

	protected void botonCancelar() {
		tabla.clearSelection();
		textFieldID.setEditable(true);
		textFieldID.setText("");
		textFieldID.grabFocus();
		textFieldFecha.setText("");
		textFieldGolesLocal.setText("");
		textFieldGolesVisitantes.setText("");
		comboBoxListaEquipoLocal.setSelectedIndex(-1);
		comboBoxListaEquipoVisitante.setSelectedIndex(-1);

	}

	@SuppressWarnings({ "serial" })
	private void mostrarTabla(List<Partido> partidos) {

		String[] tituloColumna = { "ID", "Fecha", "Local", "Visitante", "Competición", "Goles Locales",
				"Goles Visitante" };

		try {
			tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tabla.setModel(new DefaultTableModel(equiposAMatriz(partidos), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}

			});

		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

	}

	private String[][] equiposAMatriz(List<Partido> partidos2) {
		String[][] resultado = new String[partidos2.size()][7];

		for (int i = 0; i < partidos2.size(); i++) {
			resultado[i][0] = String.valueOf(partidos2.get(i).getId());
			resultado[i][1] = Utiles.convertirDateToString(partidos2.get(i).getFechaHora(), true);
			resultado[i][2] = String.valueOf(partidos2.get(i).getEquipoByIdLocal().getNombre());
			resultado[i][3] = String.valueOf(partidos2.get(i).getEquipoByIdVisitante().getNombre());
			resultado[i][4] = String.valueOf(partidos2.get(i).getCompeticion().getNombre());
			resultado[i][5] = String.valueOf(partidos2.get(i).getGolesLocal());
			resultado[i][6] = String.valueOf(partidos2.get(i).getGolesVisitante());

		}

		return resultado;
	}

	private void botonAceptar() {

		if (chckbxEliminar.isSelected()) {

			borrar();

		} else {

			if (tabla.getSelectedRow() == -1) {
				guardar();
			} else {
				modificar();
			}

		}

	}

	private void modificar() {

		Competicion competicionSeleccionada = listaCompeticiones.get(comboBoxCompeticiones.getSelectedIndex());

		String fechaString = textFieldFecha.getText();

		if (fechaString.isEmpty() || !fechaString.matches(".{4}[-].{2}[-].{2}[ ].{2}[:].{2}[:].{2}")) {
			Utiles.notificaError(null, "Error en la fecha", null, "Introduzca un formato de fecha correcto");
			return;
		}

		int posicionEquipoLocal = comboBoxListaEquipoLocal.getSelectedIndex();
		int posicionEquipoVisitante = comboBoxListaEquipoVisitante.getSelectedIndex();

		if (posicionEquipoLocal == posicionEquipoVisitante) {
			Utiles.notificaError(null, "Error con los equipos", null,
					"Seleccione equipos local y visitante diferentes");
			return;
		}

		if (posicionEquipoLocal == -1 || posicionEquipoVisitante == -1) {
			Utiles.notificaError(null, "Error con los equipos", null, "Seleccione equipos local y visitante validos");
			return;
		}

		String golesLocalString = textFieldGolesLocal.getText();
		String golesVisitanteString = textFieldGolesVisitantes.getText();

		if (!golesLocalString.matches("[0-9]*") || !golesVisitanteString.matches("[0-9]*")) {
			Utiles.notificaError(null, "Error con los goles", null, "Introduzca unos goles numéricos");
			return;
		}

		Date fecha = null;

		try {
			fecha = Utiles.formatearStringToDate(fechaString, true);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error con la fecha", null, "Introduzca una fecha válida");
			return;
		}

		if (fecha.getTime() > competicionSeleccionada.getFechaFin().getTime()) {
			Utiles.notificaError(null, "Error con los fecha", null,
					"La fecha introducida excede el fin de la competición");
			return;
		}

		if (fecha.getTime() < competicionSeleccionada.getFechaComienzo().getTime()) {
			Utiles.notificaError(null, "Error con los fecha", null,
					"La fecha introducida es inferior el comienzo de la competición");
			return;
		}

		Partido nuevoPartidoModificado = partidos.get(tabla.getSelectedRow());

		if (!nuevoPartidoModificado.getCompeticion().equals(competicionSeleccionada)) {
			Utiles.notificaError(null, "Error con la competición", null,
					"No se admite el cambio de competición en partidos");
			return;
		}

		int golesLocal;
		int golesVisitante;

		if (golesLocalString.isEmpty()) {
			golesLocal = 0;
		} else {
			golesLocal = Integer.valueOf(golesLocalString);
		}

		if (golesVisitanteString.isEmpty()) {
			golesVisitante = 0;
		} else {
			golesVisitante = Integer.valueOf(golesVisitanteString);
		}

		Equipo equipoLocal = listaEquipos.get(posicionEquipoLocal);
		Equipo equipoVisitante = listaEquipos.get(posicionEquipoVisitante);

		try {

			nuevoPartidoModificado.setEquipoByIdLocal(equipoLocal);
			nuevoPartidoModificado.setEquipoByIdVisitante(equipoVisitante);
			nuevoPartidoModificado.setFechaHora(fecha);
			nuevoPartidoModificado.setGolesLocal(golesLocal);
			nuevoPartidoModificado.setGolesVisitante(golesVisitante);
			if (JOptionPane.showConfirmDialog(null,
					"¿Estás seguro de modificar el partido con id: '" + nuevoPartidoModificado.getId() + "'?",
					"modificar partido", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
				bd.modificarDatosByID(nuevoPartidoModificado);
				filtrarPartidos();
			} else {
				return;
			}

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al modificar", e1,
					"Se ha producido un error al modificar el Partido en la base de datos");
		}

		buscar();
	}

	private void guardar() {

		if (comboBoxCompeticiones.getSelectedIndex() == -1) {
			Utiles.notificaError(null, "Error", null, "Seleccione una competicion");
			return;
		}

		Competicion competicionSeleccionada = listaCompeticiones.get(comboBoxCompeticiones.getSelectedIndex());

		String fechaString = textFieldFecha.getText();

		if (fechaString.isEmpty() || !fechaString.matches(".{4}[-].{2}[-].{2}[ ].{2}[:].{2}[:].{2}")) {
			Utiles.notificaError(null, "Error en la fecha", null, "Introduzca un formato de fecha correcto");
			return;
		}

		int posicionEquipoLocal = comboBoxListaEquipoLocal.getSelectedIndex();
		int posicionEquipoVisitante = comboBoxListaEquipoVisitante.getSelectedIndex();

		if (posicionEquipoLocal == posicionEquipoVisitante) {
			Utiles.notificaError(null, "Error con los equipos", null,
					"Seleccione equipos local y visitante diferentes");
			return;
		}

		if (posicionEquipoLocal == -1 || posicionEquipoVisitante == -1) {
			Utiles.notificaError(null, "Error con los equipos", null, "Seleccione equipos local y visitante validos");
			return;
		}

		String golesLocalString = textFieldGolesLocal.getText();
		String golesVisitanteString = textFieldGolesVisitantes.getText();

		if (!golesLocalString.matches("[0-9]*") || !golesVisitanteString.matches("[0-9]*")) {
			Utiles.notificaError(null, "Error con los goles", null, "Introduzca unos goles numéricos");
			return;
		}

		Date fecha = null;

		try {
			fecha = Utiles.formatearStringToDate(fechaString, true);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error con la fecha", null, "Introduzca una fecha válida");
			return;
		}

		if (fecha.getTime() > competicionSeleccionada.getFechaFin().getTime()) {
			Utiles.notificaError(null, "Error con los fecha", null,
					"La fecha introducida excede el fin de la competición");
			return;
		}

		if (fecha.getTime() < competicionSeleccionada.getFechaComienzo().getTime()) {
			Utiles.notificaError(null, "Error con los fecha", null,
					"La fecha introducida es inferior el comienzo de la competición");
			return;
		}

		int golesLocal;
		int golesVisitante;

		if (golesLocalString.isEmpty()) {
			golesLocal = 0;
		} else {
			golesLocal = Integer.valueOf(golesLocalString);
		}

		if (golesVisitanteString.isEmpty()) {
			golesVisitante = 0;
		} else {
			golesVisitante = Integer.valueOf(golesVisitanteString);
		}

		Equipo equipoLocal = listaEquipos.get(posicionEquipoLocal);
		Equipo equipoVisitante = listaEquipos.get(posicionEquipoVisitante);

		try {
			Partido nuevoPartido = new Partido(equipoLocal, equipoVisitante, competicionSeleccionada, fecha, golesLocal,
					golesVisitante);

			if (partidos.contains(nuevoPartido)) {
				Utiles.notificaError(null, "Partido ya existe", null, "El Partido ya existe");
				return;
			}

			bd.guardarDatos(nuevoPartido);
			filtrarPartidos();

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al guardar", e1,
					"Se ha producido un error al guardar el Partido en la base de datos");
		}
	}

	private void borrar() {
		if (tabla.getSelectedRow() == -1) {
			Utiles.notificaError(null, "Error al eliminar", null, "Para eliminar seleccione un partido en la tabla");
			return;
		}

		Partido partidoSeleccionada = partidos.get(tabla.getSelectedRow());

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de eliminar el partido con id: '" + partidoSeleccionada.getId() + "'?",
				"Eliminar partido", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				bd.borrarDatos(partidos.get(tabla.getSelectedRow()));

				filtrarPartidos();

			} catch (Exception e1) {
				Utiles.notificaError(null, "Error al borrar", e1,
						"Se ha producido un error al borrar la posicion en la base de datos");
			}
		}

	}

	private void buscar() {

		if (textFieldID.getText().isEmpty()) {
			Utiles.notificaError(null, "Error al buscar", null, "Introduzca un ID de partido");
			textFieldID.grabFocus();
			return;
		}

		Partido partidobusqueda;
		try {
			partidobusqueda = (Partido) bd.consultarDatoByID(Partido.class, textFieldID.getText());
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al buscar", e,
					"Se ha producido un error al buscar el partido en la base de datos");
			return;
		}

		if (partidobusqueda == null) {
			Utiles.notificaError(null, "Resultado Busqueda", null, "No se ha encontrado el partido");
			return;
		}

		int posicion = listaCompeticiones.lastIndexOf(partidobusqueda.getCompeticion());

		if (posicion != -1) {
			comboBoxCompeticiones.setSelectedIndex(posicion);

			int posicionPartido = partidos.lastIndexOf(partidobusqueda);

			tabla.setRowSelectionInterval(posicionPartido, posicionPartido);
		} else {
			Utiles.notificaError(null, "Competicion no encontrada", null, "No se ha encontrado la Competicion: "
					+ partidobusqueda.getCompeticion().getNombre() + " del partido con ID: " + partidobusqueda.getId());
		}
	}

	@SuppressWarnings("unchecked")
	private void filtrarPartidos() {
		try {
			partidos = (ArrayList<Partido>) bd.consultarDatosByObjetos(Partido.class, false,
					(Competicion) listaCompeticiones.get(comboBoxCompeticiones.getSelectedIndex()));
		} catch (Exception e1) {
			Utiles.notificaError(null, "Error a listar", e1, "Se ha producido un error al listar los partidos");
			return;
		}
		mostrarTabla(partidos);
		rellenarComboBoxEquipos();
	}
}
