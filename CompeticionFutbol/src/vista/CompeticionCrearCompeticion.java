package vista;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import clasesContenedoras.Competicion;
import clasesContenedoras.Equipo;
import clasesContenedoras.Partido;
import controladesConexion.Campo;
import interfacesImplementacion.Persistencia;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class CompeticionCrearCompeticion extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Persistencia bd;
	private DefaultListModel modelListaEquipos;
	private DefaultListModel modelListaEquiposCompeticion;
	private ArrayList<Equipo> listaEquipos;
	private ArrayList<Equipo> listaEquiposCompeticion;
	private ArrayList<Competicion> listaCompeticiones;
	private JList listEquiposCompeticion;
	private JList listEquipos;
	private JComboBox comboBox;
	private JTextField textFieldPeriodicidad;

	/**
	 * Create the frame.
	 */
	public CompeticionCrearCompeticion(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		modelListaEquipos = new DefaultListModel<>();
		modelListaEquiposCompeticion = new DefaultListModel<>();
		listaEquiposCompeticion = new ArrayList<>();
		listaCompeticiones = new ArrayList<>();

		setBounds(100, 100, 647, 517);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCrearCompeticin = new JLabel("Crear Competición:");
		lblCrearCompeticin.setBounds(12, 12, 230, 15);
		contentPane.add(lblCrearCompeticin);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 99, 230, 281);
		contentPane.add(scrollPane);

		listEquipos = new JList();
		scrollPane.setViewportView(listEquipos);
		listEquipos.setModel(modelListaEquipos);

		JLabel lblEquipos = new JLabel("Equipos:");
		lblEquipos.setBounds(12, 72, 151, 15);
		contentPane.add(lblEquipos);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(403, 99, 227, 278);
		contentPane.add(scrollPane_1);

		listEquiposCompeticion = new JList();
		scrollPane_1.setViewportView(listEquiposCompeticion);
		listEquiposCompeticion.setModel(modelListaEquiposCompeticion);

		JLabel lblEquiposParaCompeticin = new JLabel("Equipos para competición");
		lblEquiposParaCompeticin.setBounds(403, 72, 227, 15);
		contentPane.add(lblEquiposParaCompeticin);

		JButton btnAadir = new JButton("Añadir");
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				anadirEquipo();
			}
		});
		btnAadir.setBounds(266, 150, 117, 25);
		contentPane.add(btnAadir);

		JButton Vaciar = new JButton("vaciar");
		Vaciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciarLista();
			}
		});
		Vaciar.setBounds(266, 196, 117, 25);
		contentPane.add(Vaciar);

		JLabel lblCompeticin = new JLabel("Competición:");
		lblCompeticin.setBounds(12, 39, 140, 15);
		contentPane.add(lblCompeticin);

		comboBox = new JComboBox();
		comboBox.setBounds(170, 34, 239, 26);
		contentPane.add(comboBox);

		JLabel lblPeridiocidaddas = new JLabel("Periodicidad (Días)");
		lblPeridiocidaddas.setBounds(12, 411, 151, 15);
		contentPane.add(lblPeridiocidaddas);

		textFieldPeriodicidad = new JTextField();
		textFieldPeriodicidad.setBounds(181, 409, 162, 19);
		contentPane.add(textFieldPeriodicidad);
		textFieldPeriodicidad.setColumns(10);

		JButton btnGenerarCompeticin = new JButton("Generar Competición");
		btnGenerarCompeticin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				generarCompeticion();
			}
		});
		btnGenerarCompeticin.setBounds(12, 456, 207, 25);
		contentPane.add(btnGenerarCompeticin);

		rellenarListaJugadores();
		rellenarCompeticiones();
	}

	@SuppressWarnings("unchecked")
	protected void generarCompeticion() {

		if (comboBox.getSelectedIndex() == -1) {
			Utiles.notificaError(null, "Error", null, "Seleccione una competicion");
			return;
		}

		String periodicidad = textFieldPeriodicidad.getText();
		Competicion competicion = listaCompeticiones.get(comboBox.getSelectedIndex());

		try {
			if (bd.consultarDatosByObjetos(Partido.class, false, competicion).size() != 0) {
				Utiles.notificaError(null, "Competicion erronea", null, "La competición ya tiene partidos asociados");
				return;
			}
		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al consultar", e1,
					"Se ha producido un error al consultar la base de datos");
			return;
		}

		// >>>>> VALIDACION >>>>>
		if (!periodicidad.matches("[0-9]+")) {
			Utiles.notificaError(null, "Error en Periodicidad", null,
					"Introduzca una periodicidad válida (Solo números)");
			return;
		}

		if (listaEquiposCompeticion.isEmpty()) {
			Utiles.notificaError(null, "Error con los equipos", null,
					"Seleccione los equipos que desea introducir en la competición");
			return;
		}

		if (listaEquiposCompeticion.size() % 2 != 0) {
			Utiles.notificaError(null, "Error con los equipos", null, "Introduzca un número par de equipos");
			return;
		}

		Calendar c = Calendar.getInstance();
		c.setTime(competicion.getFechaComienzo());
		c.add(Calendar.DATE,
				(listaEquiposCompeticion.size() / 2 * listaEquiposCompeticion.size()) * Integer.valueOf(periodicidad)
						- 1);

		// System.out.println(competicion.getFechaFin() + "\n" + c.getTime());

		if (competicion.getFechaFin().getTime() < c.getTimeInMillis()) {
			Utiles.notificaError(null, "Error con la competición", null,
					"La fecha final de a periodicidad es mayor a la fecha fin de la competición");
			return;
		}

		// <<<<< VALIDACION <<<<<

		Collections.shuffle(listaEquiposCompeticion);

		boolean esPrimeraVezFecha = true;
		for (int i = 0; i < listaEquiposCompeticion.size() - 1; i++) {
			ArrayList<Partido> partidosLocal = null;
			ArrayList<Partido> partidosVisitante = null;

			for (int j = 1 + i; j < listaEquiposCompeticion.size(); j++) {

				Partido partido = null;

				if (esPrimeraVezFecha) {
					c.setTime(competicion.getFechaComienzo());
					esPrimeraVezFecha = false;
				} else
					c.add(Calendar.DATE, Integer.valueOf(periodicidad));

				try {
					partidosLocal = (ArrayList<Partido>) bd.consultarDatosByCampos(Partido.class,
							new Campo("idLocal", listaEquiposCompeticion.get(i)),
							new Campo("idComp", competicion, true));
				} catch (Exception e) {
					Utiles.notificaError(null, "Error al listar partidos", e,
							"Se ha producido un error al listar la tabla Partido");
					e.printStackTrace();
					return;
				}

				try {
					partidosVisitante = (ArrayList<Partido>) bd.consultarDatosByCampos(Partido.class,
							new Campo("idVisitante", listaEquiposCompeticion.get(i)),
							new Campo("idComp", competicion, true));
				} catch (Exception e) {
					Utiles.notificaError(null, "Error al listar partidos", e,
							"Se ha producido un error al listar la tabla Partido");
					return;
				}

				if (partidosLocal.size() <= partidosVisitante.size()) {
					partido = new Partido(listaEquiposCompeticion.get(i), listaEquiposCompeticion.get(j), competicion,
							c.getTime(), 0, 0);
				} else {
					partido = new Partido(listaEquiposCompeticion.get(j), listaEquiposCompeticion.get(i), competicion,
							c.getTime(), 0, 0);
				}

				try {
					bd.guardarDatos(partido);
				} catch (Exception e) {
					Utiles.notificaError(null, "Error al guardar", e,
							"Se ha producido un error al guardar el partido en la base de datos");
					return;
				}

			}
			// System.out.print(partidosLocal.size() + "////");
			// System.out.println(partidosVisitante.size());
		}
		Utiles.notificaError(null, "Terminado", null, "Se ha terminado de crear la competición");
	}

	@SuppressWarnings("unchecked")
	private void rellenarCompeticiones() {

		try {
			listaCompeticiones = (ArrayList<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar las Competiciones");
			return;
		}

		for (Competicion competicion : listaCompeticiones) {
			comboBox.addItem(competicion.getNombre());
		}

	}

	protected void vaciarLista() {
		modelListaEquiposCompeticion.removeAllElements();
	}

	@SuppressWarnings("unchecked")
	protected void anadirEquipo() {

		int[] seleccionados = listEquipos.getSelectedIndices();

		for (int i = 0; i < seleccionados.length; i++) {
			if (!listaEquiposCompeticion.contains(listaEquipos.get(seleccionados[i]))) {
				modelListaEquiposCompeticion.addElement(listaEquipos.get(seleccionados[i]).getNombre());
				listaEquiposCompeticion.add(listaEquipos.get(seleccionados[i]));
			}
		}

	}

	@SuppressWarnings("unchecked")
	private void rellenarListaJugadores() {

		try {
			listaEquipos = (ArrayList<Equipo>) bd.listar(Equipo.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar los equipos");
			return;
		}

		for (Equipo equipo : listaEquipos) {
			modelListaEquipos.addElement(equipo.getNombre());
		}

	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			dispose();

		}
	}
}
