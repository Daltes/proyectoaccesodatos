package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Jugador;
import clasesContenedoras.Posicion;
import interfacesImplementacion.Persistencia;

public class CRUDPosiciones extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tabla;
	private Persistencia bd;
	private JTextField nombrePosiciones;
	private JCheckBox chckbxEliminar;
	private List<Posicion> posiciones;
	private JButton btnCancelar;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public CRUDPosiciones(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		try {
			posiciones = (List<Posicion>) bd.listar(Posicion.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion ", e1,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			e1.printStackTrace();
			return;
		}

		setBounds(100, 100, 517, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 124, 491, 105);
		contentPane.add(scrollPane);

		tabla = new JTable();
		scrollPane.setViewportView(tabla);

		mostrarTabla();

		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				cambiarNombreSeleccionado();
			}
		});

		JLabel lblNombreDelEquipo = new JLabel("Nombre del Equipo");
		lblNombreDelEquipo.setBounds(12, 42, 151, 15);
		contentPane.add(lblNombreDelEquipo);

		nombrePosiciones = new JTextField();
		nombrePosiciones.setBounds(171, 40, 332, 19);
		contentPane.add(nombrePosiciones);
		nombrePosiciones.setColumns(10);

		JButton btnGuardar = new JButton("Aceptar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonAceptar();
			}
		});
		btnGuardar.setBounds(386, 87, 117, 25);
		contentPane.add(btnGuardar);

		chckbxEliminar = new JCheckBox("Eliminar");
		chckbxEliminar.setBounds(12, 93, 108, 23);
		contentPane.add(chckbxEliminar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonCancelar();
			}
		});
		btnCancelar.setBounds(261, 87, 117, 25);
		contentPane.add(btnCancelar);

		btnCancelar.setBounds(261, 87, 117, 25);
		contentPane.add(btnCancelar);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int posicion = posiciones.lastIndexOf(new Posicion(nombrePosiciones.getText()));

				if (posicion != -1) {
					tabla.setRowSelectionInterval(posicion, posicion);
				} else {
					Utiles.notificaError(null, "Posicion no encontrada", null,
							"No se ha encontrado la Posicion: " + nombrePosiciones.getText());
				}
			}
		});
		btnBuscar.setBounds(132, 87, 117, 25);
		contentPane.add(btnBuscar);

	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			this.dispose();

		}
	}

	protected void cambiarNombreSeleccionado() {
		if (tabla.getSelectedRow() != -1) {
			nombrePosiciones.setText(posiciones.get(tabla.getSelectedRow()).getDescripcion());
		}

	}

	protected void botonCancelar() {
		tabla.clearSelection();
		nombrePosiciones.setText("");
		nombrePosiciones.grabFocus();
	}

	@SuppressWarnings({ "serial", "unchecked" })
	private void mostrarTabla() {

		posiciones.clear();

		try {
			posiciones = (List<Posicion>) bd.listar(Posicion.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", e1,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

		String[] tituloColumna = { "ID", "Descrición" };

		try {
			tabla.setModel(new DefaultTableModel(equiposAMatriz(posiciones), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

		tabla.grabFocus();
		if (tabla.getRowCount() != 0)
			tabla.setRowSelectionInterval(0, 0);
		tabla.setSelectionMode(0);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.getColumnModel().getColumn(0).setPreferredWidth(35);
		tabla.getColumnModel().getColumn(1).setMinWidth(453);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

	}

	private String[][] equiposAMatriz(List<Posicion> posiciones2) {
		String[][] resultado = new String[posiciones2.size()][2];

		for (int i = 0; i < posiciones2.size(); i++) {
			resultado[i][0] = String.valueOf(posiciones2.get(i).getId());
			resultado[i][1] = String.valueOf(posiciones2.get(i).getDescripcion());
		}

		return resultado;
	}

	private void botonAceptar() {

		if (chckbxEliminar.isSelected()) {

			borrar();

		} else {
			if (!nombrePosiciones.getText().isEmpty()) {

				if (tabla.getSelectedRow() == -1) {
					guardar();
				} else {
					modificar();
				}

			} else {
				Utiles.notificaError(null, "Nombre vacío", null, "Introduzca un nombre para dar de alta");
			}

		}

		botonCancelar();

	}

	private void modificar() {

		Posicion posicionSeleccionado = posiciones.get(tabla.getSelectedRow());

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de modificar el equipo: '" + posicionSeleccionado.getDescripcion() + "'?",
				"Modificar equipo", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				posicionSeleccionado.setDescripcion(nombrePosiciones.getText());
				bd.modificarDatosByID(posicionSeleccionado);
				mostrarTabla();
			} catch (Exception e) {
				Utiles.notificaError(null, "Error al modificar", e, "Se ha producido un error al modificar");
			}
		}

	}

	private void guardar() {
		try {
			Posicion nuevaPosicion = new Posicion(nombrePosiciones.getText());

			if (posiciones.contains(nuevaPosicion)) {
				Utiles.notificaError(null, "Equipo ya existe", null, "El equipo ya existe");
				return;
			}

			bd.guardarDatos(nuevaPosicion);
			posiciones.add(nuevaPosicion);
			mostrarTabla();
			tabla.revalidate();
			tabla.repaint();

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al guardar", e1,
					"Se ha producido un error al guardar la posicion en la base de datos");
		}
	}

	private void borrar() {
		if (tabla.getSelectedRow() == -1) {
			Utiles.notificaError(null, "Error al eliminar", null, "Para eliminar seleccione una posicion en la tabla");
			return;
		}

		Posicion posicionSeleccionada = posiciones.get(tabla.getSelectedRow());

		try {
			@SuppressWarnings("unchecked")
			ArrayList<Jugador> jugadores = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class, true,
					posicionSeleccionada);

			if (!jugadores.isEmpty()) {
				Utiles.notificaError(null, "No se puede borrar", null,
						"Hay jugadores con la posicion que desea borrar, elimine los jugadores asociados");
				return;
			}

		} catch (Exception e) {
			Utiles.notificaError(null, "Error al borrar", e,
					"Se ha producido un error al comprobar las posiciones en la base de datos");
			return;
		}

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de eliminar el equipo: '" + posicionSeleccionada.getDescripcion() + "'?",
				"Eliminar Posicion", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				bd.borrarDatos(posiciones.get(tabla.getSelectedRow()));
				mostrarTabla();
			} catch (Exception e1) {
				Utiles.notificaError(null, "Error al borrar", e1,
						"Se ha producido un error al borrar la posicion en la base de datos");
			}
		}

	}

}
