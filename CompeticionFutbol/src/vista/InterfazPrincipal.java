package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controladesConexion.PersistenciaHibernate;
import controladesConexion.PersistenciaJDBC;
import interfacesImplementacion.Persistencia;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.awt.event.ActionEvent;

public class InterfazPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Persistencia bd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazPrincipal frame = new InterfazPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazPrincipal() {

		bd = null;

		try {
			File f = new File("src/vista/CFG.INI");

			Properties prop = new Properties();
			prop.load(new FileReader(f));

			String tipoPer = prop.getProperty("tipoPersistencia");
			if (tipoPer.equals("mysqlJDBC")) {
				bd = new PersistenciaJDBC();
			} else {
				bd = new PersistenciaHibernate();
			}

			bd.conectar(new FileReader(f));

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al conectar", e1, "No se ha podido conectar con la base de datos");
			System.exit(-1);
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 594, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMantenimiento = new JMenu("Mantenimiento");
		menuBar.add(mnMantenimiento);

		JMenuItem mntmEquipos = new JMenuItem("Equipos");
		mntmEquipos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CRUDEquipos ventana = new CRUDEquipos(bd);
				ventana.setVisible(true);
			}
		});
		mnMantenimiento.add(mntmEquipos);

		JMenuItem mntmPosiciones = new JMenuItem("Posiciones");
		mntmPosiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CRUDPosiciones ventana = new CRUDPosiciones(bd);
				ventana.setVisible(true);
			}
		});
		mnMantenimiento.add(mntmPosiciones);

		JMenuItem mntmCompeticiones = new JMenuItem("Competiciones");
		mntmCompeticiones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CRUDCompeticiones ventana = new CRUDCompeticiones(bd);
				ventana.setVisible(true);
			}
		});
		mnMantenimiento.add(mntmCompeticiones);

		JMenu mnGestinDeJugadores = new JMenu("Gestión de Jugadores");
		menuBar.add(mnGestinDeJugadores);

		JMenuItem mntmJugadoresDeUn = new JMenuItem("Gestión de Jugadores");
		mntmJugadoresDeUn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GestionJugadoresEquipos ventana = new GestionJugadoresEquipos(bd);
				ventana.setVisible(true);
			}
		});
		mnGestinDeJugadores.add(mntmJugadoresDeUn);

		JMenu mnCompeticin = new JMenu("Competición");
		menuBar.add(mnCompeticin);

		JMenuItem mntmCrearCompeticin = new JMenuItem("Crear competición");
		mntmCrearCompeticin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CompeticionCrearCompeticion ventana = new CompeticionCrearCompeticion(bd);
				ventana.setVisible(true);
			}
		});
		mnCompeticin.add(mntmCrearCompeticin);

		JMenuItem mntmGestinDePartidos = new JMenuItem("Gestión de partidos");
		mntmGestinDePartidos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				CompeticionGestionarPartidos ventana = new CompeticionGestionarPartidos(bd);
				ventana.setVisible(true);

			}
		});
		mnCompeticin.add(mntmGestinDePartidos);

		JMenu mnInformes = new JMenu("Informes");
		menuBar.add(mnInformes);

		JMenuItem mntmClasificacin = new JMenuItem("Clasificación");
		mntmClasificacin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				InformesClasificacion ventana = new InformesClasificacion(bd);
				ventana.setVisible(true);

			}
		});
		mnInformes.add(mntmClasificacin);

		JMenuItem mntmPichichi = new JMenuItem("Pichichi");
		mntmPichichi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				InformesPichichi ventana = new InformesPichichi(bd);
				ventana.setVisible(true);

			}
		});
		mnInformes.add(mntmPichichi);

		JMenuItem mntmleero = new JMenuItem("'Leñero'");
		mntmleero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				InformesLenero ventana = new InformesLenero(bd);
				ventana.setVisible(true);

			}
		});
		mnInformes.add(mntmleero);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
