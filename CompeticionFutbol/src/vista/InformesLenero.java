package vista;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Competicion;
import clasesContenedoras.Estadistica;
import clasesContenedoras.Partido;
import interfacesImplementacion.Persistencia;
import javax.swing.JComboBox;

public class InformesLenero extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tablaRojas;
	private Persistencia bd;
	private List<Partido> partidos;
	private ArrayList<Competicion> listaCompeticiones;
	private JComboBox comboBoxCompeticiones;
	private Map<String, Integer> listaClasificacionRojas;
	private Map<String, Integer> listaClasificacionAmarilla;
	private Map<String, Integer> listaClasificacionFaltas;
	private JLabel lblTarjetasRojas;
	private JLabel lblTarjetasAmarillas;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JTable tablaAmarilla;
	private JTable tablaFaltas;

	/**
	 * Create the frame.
	 */
	public InformesLenero(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		listaClasificacionRojas = new TreeMap<>();
		listaClasificacionAmarilla = new TreeMap<>();
		listaClasificacionFaltas = new TreeMap<>();

		setBounds(100, 100, 524, 731);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 101, 492, 160);
		contentPane.add(scrollPane);

		tablaRojas = new JTable();
		scrollPane.setViewportView(tablaRojas);

		JLabel lblCompeticin = new JLabel("Competición");
		lblCompeticin.setBounds(6, 30, 108, 14);
		contentPane.add(lblCompeticin);

		comboBoxCompeticiones = new JComboBox();
		comboBoxCompeticiones.setBounds(137, 28, 198, 18);
		contentPane.add(comboBoxCompeticiones);

		lblTarjetasRojas = new JLabel("Tarjetas Rojas");
		lblTarjetasRojas.setBounds(6, 76, 108, 14);
		contentPane.add(lblTarjetasRojas);

		lblTarjetasAmarillas = new JLabel("Tarjetas Amarillas");
		lblTarjetasAmarillas.setBounds(6, 272, 108, 14);
		contentPane.add(lblTarjetasAmarillas);

		JLabel lblFaltas = new JLabel("Faltas");
		lblFaltas.setBounds(6, 468, 108, 14);
		contentPane.add(lblFaltas);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(6, 297, 492, 160);
		contentPane.add(scrollPane_1);

		tablaAmarilla = new JTable();
		scrollPane_1.setColumnHeaderView(tablaAmarilla);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(6, 493, 492, 160);
		contentPane.add(scrollPane_2);

		tablaFaltas = new JTable();
		scrollPane_2.setColumnHeaderView(tablaFaltas);

		comboBoxCompeticiones.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					filtrarPartidos();
				}
			}
		});

		rellenarComboBoxCompeticiones();
	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxCompeticiones() {

		try {
			listaCompeticiones = (ArrayList<Competicion>) bd.listar(Competicion.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar las competiciones");
			return;
		}

		for (Competicion competicion : listaCompeticiones) {
			comboBoxCompeticiones.addItem(competicion.getNombre());
		}
	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			this.dispose();

		}
	}

	@SuppressWarnings({ "serial" })
	private void mostrarTabla(LinkedHashMap<String, Integer> resultadoRojas,
			LinkedHashMap<String, Integer> resultadoAmarillas, LinkedHashMap<String, Integer> resultadoFaltas) {

		String[] tituloColumna = { "Posicion", "Nombre Jugador", "Rojas" };

		try {
			tablaRojas.setModel(new DefaultTableModel(equiposAMatriz(resultadoRojas), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

		String[] tituloColumnaAmarillas = { "Posicion", "Nombre Jugador", "Amarillas" };

		try {
			tablaAmarilla.setModel(new DefaultTableModel(equiposAMatriz(resultadoAmarillas), tituloColumnaAmarillas) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

		String[] tituloColumnaFaltas = { "Posicion", "Nombre Jugador", "Faltas" };

		try {
			tablaFaltas.setModel(new DefaultTableModel(equiposAMatriz(resultadoFaltas), tituloColumnaFaltas) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Posicion", ex,
					"Se ha producido un erro al listar la tabla 'Posicion'");
			return;
		}

	}

	private String[][] equiposAMatriz(LinkedHashMap<String, Integer> resultado2) {
		String[][] resultado = new String[resultado2.size()][3];
		ArrayList<String> claves = new ArrayList<String>(resultado2.keySet());
		int j = 0;
		for (int i = claves.size() - 1; i >= 0; i--) {
			// for (int i = 0; i < claves.size(); i++) {
			resultado[j][0] = String.valueOf(i + 1);
			resultado[j][1] = claves.get(i);
			resultado[j][2] = resultado2.get(claves.get(i)).toString();
			j++;
		}

		return resultado;
	}

	@SuppressWarnings("unchecked")
	private void filtrarPartidos() {
		try {
			partidos = (ArrayList<Partido>) bd.consultarDatosByObjetos(Partido.class, false,
					(Competicion) listaCompeticiones.get(comboBoxCompeticiones.getSelectedIndex()));
		} catch (Exception e1) {
			Utiles.notificaError(null, "Error a listar", e1, "Se ha producido un error al listar los partidos");
			return;
		}

		listaClasificacionRojas.clear();
		listaClasificacionFaltas.clear();
		listaClasificacionFaltas.clear();

		for (Partido partido : partidos) {
			ArrayList<Estadistica> estadisticas;
			try {
				estadisticas = (ArrayList<Estadistica>) bd.consultarDatosByObjetos(Estadistica.class, true, partido);
			} catch (Exception e) {
				Utiles.notificaError(null, "Error a listar", e, "Se ha producido un error al listar las estadisticas");
				return;
			}

			for (Estadistica estadistica : estadisticas) {

				int nuevosRojas = 0;

				if (listaClasificacionRojas.containsKey(estadistica.getJugador().getNombre())) {
					int valorActual = listaClasificacionRojas.get(estadistica.getJugador().getNombre());
					nuevosRojas = estadistica.getTarjRojas();
					valorActual += nuevosRojas;

					listaClasificacionRojas.put(estadistica.getJugador().getNombre(), valorActual);

				} else {
					nuevosRojas = estadistica.getTarjRojas();
					listaClasificacionRojas.put(estadistica.getJugador().getNombre(), nuevosRojas);
				}

				int nuevosAmarillas = 0;

				if (listaClasificacionAmarilla.containsKey(estadistica.getJugador().getNombre())) {
					int valorActual = listaClasificacionAmarilla.get(estadistica.getJugador().getNombre());
					nuevosAmarillas = estadistica.getTarjAmarillas();
					valorActual += nuevosAmarillas;

					listaClasificacionAmarilla.put(estadistica.getJugador().getNombre(), valorActual);

				} else {
					nuevosAmarillas = estadistica.getTarjAmarillas();
					listaClasificacionAmarilla.put(estadistica.getJugador().getNombre(), nuevosAmarillas);
				}

				int nuevosfaltas = 0;

				if (listaClasificacionFaltas.containsKey(estadistica.getJugador().getNombre())) {
					int valorActual = listaClasificacionFaltas.get(estadistica.getJugador().getNombre());
					nuevosfaltas = estadistica.getFaltas();
					valorActual += nuevosfaltas;

					listaClasificacionFaltas.put(estadistica.getJugador().getNombre(), valorActual);

				} else {
					nuevosfaltas = estadistica.getFaltas();
					listaClasificacionFaltas.put(estadistica.getJugador().getNombre(), nuevosfaltas);
				}

			}

		}

		LinkedHashMap<String, Integer> resultadoRojas = (LinkedHashMap<String, Integer>) Utiles
				.sortByValue(listaClasificacionRojas);

		LinkedHashMap<String, Integer> resultadoAmarillas = (LinkedHashMap<String, Integer>) Utiles
				.sortByValue(listaClasificacionAmarilla);

		LinkedHashMap<String, Integer> resultadofaltas = (LinkedHashMap<String, Integer>) Utiles
				.sortByValue(listaClasificacionFaltas);

		/*
		 * Map<String, Integer> prueba = new TreeMap<>(); prueba.put("tercero", 10);
		 * prueba.put("Primero", 1); prueba.put("Segundo", 5);
		 * 
		 * TreeMap<String, Integer> resultadoPrueba = (TreeMap<String, Integer>)
		 * Utiles.sortByValue(prueba); ArrayList<String> claves = new
		 * ArrayList<String>(resultadoPrueba.keySet()); for(int i=claves.size()-1;
		 * i>=0;i--){ System.out.println(resultadoPrueba.get(claves.get(i))); }
		 */

		// System.out.println(resultadoPrueba);

		// System.out.println(resultado);

		mostrarTabla(resultadoRojas, resultadoAmarillas, resultadofaltas);
	}
}
