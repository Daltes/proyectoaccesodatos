package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Equipo;
import clasesContenedoras.Estadistica;
import clasesContenedoras.Jugador;
import clasesContenedoras.Posicion;
import interfacesImplementacion.Persistencia;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GestionJugadoresEquipos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Persistencia bd;
	private ArrayList<Equipo> listaEquipos;
	private ArrayList<Posicion> listaPosiciones;
	private JComboBox<String> comboBoxEquipos;
	private JTable tabla;
	private ArrayList<Jugador> listaJugadores;
	private JTextField textFieldLicencia;
	private JTextField textFieldNombre;
	private JTextField textFieldDorsal;
	private JComboBox<String> comboBoxPosicionJugador;
	private JComboBox<String> comboBoxEquipoJugador;
	private JCheckBox chckbxEliminar;

	/**
	 * Create the frame.
	 */
	public GestionJugadoresEquipos(Persistencia bd) {

		this.bd = bd;
		listaJugadores = new ArrayList<>();

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1050, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEquipo = new JLabel("Equipo");
		lblEquipo.setBounds(10, 27, 61, 14);
		contentPane.add(lblEquipo);

		comboBoxEquipos = new JComboBox<>();
		comboBoxEquipos.setEditable(true);
		comboBoxEquipos.setBounds(81, 24, 228, 20);
		contentPane.add(comboBoxEquipos);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(491, 11, 533, 363);
		contentPane.add(scrollPane);

		tabla = new JTable();
		scrollPane.setViewportView(tabla);
		tabla.setSelectionMode(0);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 77, 470, 2);
		contentPane.add(separator);

		JLabel lblJugador = new JLabel("Jugador");
		lblJugador.setBounds(10, 90, 135, 14);
		contentPane.add(lblJugador);

		JButton btnListarJugadores = new JButton("Listar jugadores");

		btnListarJugadores.setBounds(10, 115, 135, 23);
		contentPane.add(btnListarJugadores);

		JLabel lblLicencia = new JLabel("Licencia");
		lblLicencia.setBounds(10, 149, 135, 14);
		contentPane.add(lblLicencia);

		textFieldLicencia = new JTextField();
		textFieldLicencia.setEditable(false);
		textFieldLicencia.setBounds(155, 146, 228, 20);
		contentPane.add(textFieldLicencia);
		textFieldLicencia.setColumns(10);

		JLabel lblEquipo_1 = new JLabel("Equipo");
		lblEquipo_1.setBounds(10, 180, 135, 14);
		contentPane.add(lblEquipo_1);

		comboBoxEquipoJugador = new JComboBox<>();
		comboBoxEquipoJugador.setBounds(155, 177, 228, 20);
		contentPane.add(comboBoxEquipoJugador);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(155, 208, 228, 20);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 211, 135, 14);
		contentPane.add(lblNombre);

		textFieldDorsal = new JTextField();
		textFieldDorsal.setBounds(155, 239, 228, 20);
		contentPane.add(textFieldDorsal);
		textFieldDorsal.setColumns(10);

		JLabel lblDorsal = new JLabel("Dorsal");
		lblDorsal.setBounds(10, 242, 135, 14);
		contentPane.add(lblDorsal);

		JLabel lblPosicion = new JLabel("Posicion");
		lblPosicion.setBounds(10, 274, 135, 14);
		contentPane.add(lblPosicion);

		comboBoxPosicionJugador = new JComboBox<>();
		comboBoxPosicionJugador.setBounds(155, 270, 228, 20);
		contentPane.add(comboBoxPosicionJugador);

		chckbxEliminar = new JCheckBox("Eliminar");
		chckbxEliminar.setBounds(6, 304, 139, 23);
		contentPane.add(chckbxEliminar);

		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonAceptar();
			}
		});
		btnAceptar.setBounds(10, 334, 89, 23);
		contentPane.add(btnAceptar);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonCancelar();
			}
		});
		btnCancelar.setBounds(109, 334, 109, 23);
		contentPane.add(btnCancelar);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarJugador();
			}
		});
		btnBuscar.setBounds(230, 334, 89, 23);
		contentPane.add(btnBuscar);

		rellenarComboBoxEquipos(comboBoxEquipoJugador);
		rellenarComboBoxPosiciones(comboBoxPosicionJugador);

		btnListarJugadores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mostrarTabla(false);
			}
		});

		comboBoxEquipos.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mostrarTabla(true);
				}
			}
		});

		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				cambiarJugadorSeleccionado();
			}
		});

		rellenarComboBoxEquipos(comboBoxEquipos);

	}

	protected void buscarJugador() {

		if (textFieldLicencia.getText().isEmpty()) {
			Utiles.notificaError(null, "Licencia vacía", null, "Introduzca una licencia para buscar a un jugador");
			return;
		}

		Jugador jugadorSeleccionado = null;
		try {
			jugadorSeleccionado = (Jugador) bd.consultarDatoByID(Jugador.class, textFieldLicencia.getText());
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al buscar", e, "Se ha producido un error al buscar en la base de datos");
			return;
		}

		if (jugadorSeleccionado == null) {
			Utiles.notificaError(null, "Resultado busqueda", null,
					"No se ha encontrado al jugador con la licencia: " + textFieldLicencia.getText());
			return;
		}

		textFieldNombre.setText(jugadorSeleccionado.getNombre());
		textFieldLicencia.setText(String.valueOf(jugadorSeleccionado.getLicencia()));
		textFieldDorsal.setText(String.valueOf(jugadorSeleccionado.getDorsal()));

		int posicionEquipo = listaEquipos.lastIndexOf(jugadorSeleccionado.getEquipo());
		comboBoxEquipoJugador.setSelectedIndex(posicionEquipo);
		comboBoxEquipos.setSelectedIndex(posicionEquipo);

		int posicionPosicion = listaPosiciones.lastIndexOf(jugadorSeleccionado.getPosicion());
		comboBoxPosicionJugador.setSelectedIndex(posicionPosicion);

		int posicionJugador = listaJugadores.indexOf(jugadorSeleccionado);
		tabla.setRowSelectionInterval(posicionJugador, posicionJugador);

	}

	protected void botonCancelar() {

		textFieldLicencia.setEditable(true);
		tabla.clearSelection();
		textFieldDorsal.setText("");
		textFieldLicencia.setText("");
		textFieldNombre.setText("");
		if (listaEquipos.size() != 0)
			comboBoxEquipoJugador.setSelectedIndex(0);
		if (listaJugadores.size() != 0)
			comboBoxPosicionJugador.setSelectedIndex(0);

	}

	protected void cambiarJugadorSeleccionado() {

		if (tabla.getSelectedRow() == -1) {
			return;
		}

		textFieldLicencia.setEditable(false);

		Jugador jugadorSeleccionado = listaJugadores.get(tabla.getSelectedRow());

		textFieldNombre.setText(jugadorSeleccionado.getNombre());
		textFieldLicencia.setText(String.valueOf(jugadorSeleccionado.getLicencia()));
		textFieldDorsal.setText(String.valueOf(jugadorSeleccionado.getDorsal()));

		int posicionEquipo = listaEquipos.lastIndexOf(jugadorSeleccionado.getEquipo());
		comboBoxEquipoJugador.setSelectedIndex(posicionEquipo);

		int posicionPosicion = listaPosiciones.lastIndexOf(jugadorSeleccionado.getPosicion());
		comboBoxPosicionJugador.setSelectedIndex(posicionPosicion);
	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxPosiciones(JComboBox<String> combobox) {

		try {
			listaPosiciones = (ArrayList<Posicion>) bd.listar(Posicion.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar las posiciones");
			return;
		}

		for (int i = 0; i < listaPosiciones.size(); i++) {
			combobox.addItem(listaPosiciones.get(i).getDescripcion());
		}
	}

	@SuppressWarnings({ "unchecked", "serial" })
	private void mostrarTabla(boolean buscarPorEquipos) {

		Equipo equipo = null;
		if (buscarPorEquipos && comboBoxEquipos.getSelectedIndex() != -1)
			equipo = listaEquipos.get(comboBoxEquipos.getSelectedIndex());
		else if (buscarPorEquipos) {
			int posicion = listaEquipos.indexOf(new Equipo((String) comboBoxEquipos.getSelectedItem()));
			if (posicion != -1)
				equipo = listaEquipos.get(posicion);
		}
		if (buscarPorEquipos && equipo == null) {
			Utiles.notificaError(null, "Notificación de equipo", null, "El equipo no existe en la base de datos");
			return;
		}

		listaJugadores.clear();

		try {
			if (buscarPorEquipos)
				listaJugadores = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class, true, equipo);
			else
				listaJugadores = (ArrayList<Jugador>) bd.listar(Jugador.class);

		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar los jugadores");
			return;
		}

		String[] titulosColumna = { "Licencia", "Nombre", "Dorsal", "Posicion" };

		try {
			tabla.setModel(new DefaultTableModel(jugadoresAMatriz(), titulosColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}

			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Jugado", ex,
					"Se ha producido un erro al listar la tabla 'Jugador'");
			ex.printStackTrace();
			return;
		}

		tabla.grabFocus();
		if (listaJugadores.size() != 0)
			tabla.setRowSelectionInterval(0, 0);

	}

	private Object[][] jugadoresAMatriz() {

		String[][] resultado = new String[listaJugadores.size()][4];

		for (int i = 0; i < listaJugadores.size(); i++) {
			resultado[i][0] = String.valueOf(listaJugadores.get(i).getLicencia());
			resultado[i][1] = listaJugadores.get(i).getNombre();

			resultado[i][2] = String.valueOf(listaJugadores.get(i).getDorsal());
			resultado[i][3] = listaJugadores.get(i).getPosicion().getDescripcion();
		}

		return resultado;
	}

	@SuppressWarnings("unchecked")
	private void rellenarComboBoxEquipos(JComboBox<String> comboBox) {
		try {
			listaEquipos = (ArrayList<Equipo>) bd.listar(Equipo.class);
		} catch (Exception e) {
			Utiles.notificaError(null, "Error al listar", e, "Se ha producido un error al listar los equipos");
			return;
		}

		for (int i = 0; i < listaEquipos.size(); i++) {
			comboBox.addItem(listaEquipos.get(i).getNombre());
		}

	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			dispose();
		}
	}

	private void botonAceptar() {

		if (chckbxEliminar.isSelected()) {

			borrar();

			botonCancelar();

		} else {
			if (textFieldLicencia.getText().isEmpty() || !textFieldLicencia.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Licencia erronea", null,
						"Introduzca una licencia para dar de alta o modificar");
				textFieldLicencia.grabFocus();
			} else if (textFieldDorsal.getText().isEmpty() || !textFieldDorsal.getText().matches("[0-9]+")) {
				Utiles.notificaError(null, "Dorsal erroneo", null,
						"Introduzca un dorsal valido para dar de alta o modificar");
				textFieldDorsal.grabFocus();
			} else if (textFieldNombre.getText().isEmpty()) {
				Utiles.notificaError(null, "Nombre erroneo", null,
						"Introduzca un nombre valido para dar de alta o modificar");
				textFieldNombre.grabFocus();
			} else {
				if (tabla.getSelectedRow() == -1) {
					guardar();
				} else {
					modificar();
				}
			}

		}
	}

	@SuppressWarnings("unchecked")
	private void modificar() {

		Equipo equipo = listaEquipos.get(comboBoxEquipoJugador.getSelectedIndex());
		Posicion posicion = listaPosiciones.get(comboBoxPosicionJugador.getSelectedIndex());
		String nombre = textFieldNombre.getText();
		int dorsal = Integer.valueOf(textFieldDorsal.getText());

		Jugador jugador = listaJugadores.get(tabla.getSelectedRow());
		Equipo equipoAntiguo = jugador.getEquipo();
		int dorsalAntiguo = jugador.getDorsal();

		try {

			int posicionEquipo = listaEquipos.lastIndexOf(equipo);

			comboBoxEquipos.setSelectedIndex(posicionEquipo);
			if ((!equipo.equals(equipoAntiguo) || dorsalAntiguo != dorsal)) {

				ArrayList<Jugador> jugadoresNuevoEquipo = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class,
						false, equipo);

				boolean datosIncorrectos = false;
				datosIncorrectos = comprobarJugador(equipo, dorsal, jugadoresNuevoEquipo);

				if (datosIncorrectos) {
					Utiles.notificaError(null, "Jugador ya existe", null,
							"El Jugador tiene el mismo dorsa otro en el equipo, o la licencia ya existe");
					return;
				}

			}

			jugador.setEquipo(equipo);
			jugador.setPosicion(posicion);
			jugador.setNombre(nombre);
			jugador.setDorsal(dorsal);

			// System.out.println("voy a modificar");
			bd.modificarDatosByID(jugador);

			mostrarTabla(true);

			int posicionJugador = listaJugadores.indexOf(jugador);
			tabla.setRowSelectionInterval(posicionJugador, posicionJugador);
		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al modicar", e1,
					"Se ha producido un error al modificar al jugador en la base de datos");
		}
	}

	private boolean comprobarJugador(Equipo equipo, int dorsal, ArrayList<Jugador> jugadoresNuevoEquipo) {
		for (int i = 0; i < jugadoresNuevoEquipo.size(); i++) {
			if (jugadoresNuevoEquipo.get(i).getDorsal() == dorsal) {
				return true;
			}
		}
		return false;
	}

	private void guardar() {

		if (comboBoxEquipoJugador.getSelectedIndex() == -1) {
			Utiles.notificaError(null, "Error", null, "Seleccione un equipo");
			return;
		}

		if (comboBoxPosicionJugador.getSelectedIndex() == -1) {
			Utiles.notificaError(null, "Error", null, "Seleccione una posicion");
			return;
		}

		try {
			int licencia = Integer.valueOf(textFieldLicencia.getText());
			Equipo equipo = listaEquipos.get(comboBoxEquipoJugador.getSelectedIndex());
			Posicion posicion = listaPosiciones.get(comboBoxPosicionJugador.getSelectedIndex());
			String nombre = textFieldNombre.getText();
			int dorsal = Integer.valueOf(textFieldDorsal.getText());

			Jugador jugadorNuevo = new Jugador(licencia, equipo, posicion, nombre, dorsal);

			int posicionEquipo = listaEquipos.lastIndexOf(jugadorNuevo.getEquipo());
			comboBoxEquipos.setSelectedIndex(posicionEquipo);
			if (listaJugadores.contains(jugadorNuevo)) {
				Utiles.notificaError(null, "Jugador ya existe", null,
						"El Jugador tiene el mismo dorsa otro en el equipo, o la licencia ya existe");
				return;
			}

			Jugador jugadorPrueba = (Jugador) bd.consultarDatoByID(Jugador.class, textFieldLicencia.getText());

			if (jugadorPrueba != null) {
				Utiles.notificaError(null, "Error", null, "La licencia ya está en uso");
				return;
			}
			bd.guardarDatos(jugadorNuevo);
			mostrarTabla(true);

			int posicionJugador = listaJugadores.indexOf(jugadorNuevo);
			tabla.setRowSelectionInterval(posicionJugador, posicionJugador);

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al guardar", e1,
					"Se ha producido un error al guardar el equipo en la base de datos");
			return;
		}

	}

	private void borrar() {
		if (tabla.getSelectedRow() == -1) {
			Utiles.notificaError(null, "Error al eliminar", null, "Para eliminar seleccione un jugador en la tabla");
			return;
		}

		Jugador jugadorSeleccionado = listaJugadores.get(tabla.getSelectedRow());

		try {
			@SuppressWarnings("unchecked")
			ArrayList<Estadistica> estadistica = (ArrayList<Estadistica>) bd.consultarDatosByObjetos(Estadistica.class,
					true, jugadorSeleccionado);

			if (!estadistica.isEmpty()) {
				Utiles.notificaError(null, "No se puede borrar", null,
						"Hay estadisticas en el jugador que desea borrar, elimine las estadisticas asociadas");
				return;
			}

		} catch (Exception e) {
			Utiles.notificaError(null, "Error al borrar", e,
					"Se ha producido un error al comprobar las estadisticas en la base de datos");
			return;
		}

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de eliminar el equipo: '" + jugadorSeleccionado.getNombre() + "'?", "Eliminar Equipo",
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				bd.borrarDatos(listaJugadores.get(tabla.getSelectedRow()));
				mostrarTabla(false);
			} catch (Exception e1) {
				Utiles.notificaError(null, "Error al borrar", e1,
						"Se ha producido un error al borrar el equipo en la base de datos");
			}
		}

	}
}
