package vista;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import clasesContenedoras.Equipo;
import clasesContenedoras.Jugador;
import clasesContenedoras.Partido;
import interfacesImplementacion.Persistencia;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class CRUDEquipos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tabla;
	private Persistencia bd;
	private JTextField nombreEquipo;
	private JCheckBox chckbxEliminar;
	private List<Equipo> equipos;
	private JButton btnCancelar;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public CRUDEquipos(Persistencia bd) {

		// Habilita la gestion de eventos de la ventana
		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);
		// Descargar el formulario y ejecutara el evento
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		this.bd = bd;

		try {
			equipos = (List<Equipo>) bd.listar(Equipo.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Equipo", e1,
					"Se ha producido un erro al listar la tabla 'Equipo'");
			e1.printStackTrace();
			return;
		}

		setBounds(100, 100, 517, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 124, 491, 105);
		contentPane.add(scrollPane);

		tabla = new JTable();
		scrollPane.setViewportView(tabla);

		mostrarTabla();

		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				cambiarNombreSeleccionado();
			}
		});

		JLabel lblNombreDelEquipo = new JLabel("Nombre del Equipo");
		lblNombreDelEquipo.setBounds(12, 42, 151, 15);
		contentPane.add(lblNombreDelEquipo);

		nombreEquipo = new JTextField();
		nombreEquipo.setBounds(171, 40, 332, 19);
		contentPane.add(nombreEquipo);
		nombreEquipo.setColumns(10);

		JButton btnGuardar = new JButton("Aceptar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonAceptar();
			}
		});
		btnGuardar.setBounds(386, 87, 117, 25);
		contentPane.add(btnGuardar);

		chckbxEliminar = new JCheckBox("Eliminar");
		chckbxEliminar.setBounds(12, 93, 103, 23);
		contentPane.add(chckbxEliminar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				botonCancelar();
			}
		});
		btnCancelar.setBounds(261, 87, 117, 25);
		contentPane.add(btnCancelar);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int posicion = equipos.lastIndexOf(new Equipo(nombreEquipo.getText()));

				if (posicion != -1) {
					tabla.setRowSelectionInterval(posicion, posicion);
				} else {
					Utiles.notificaError(null, "Equipo no encontrado", null,
							"No se ha encontrado el equipo: " + nombreEquipo.getText());
				}
			}
		});
		btnBuscar.setBounds(132, 87, 117, 25);
		contentPane.add(btnBuscar);

	}

	// Evento de cierre de ventana
	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {

			dispose();

		}
	}

	protected void cambiarNombreSeleccionado() {
		if (tabla.getSelectedRow() != -1) {
			nombreEquipo.setText(((Equipo) equipos.get(tabla.getSelectedRow())).getNombre());
		}

	}

	protected void botonCancelar() {
		tabla.clearSelection();
		nombreEquipo.setText("");
		nombreEquipo.grabFocus();
	}

	@SuppressWarnings({ "serial", "unchecked" })
	private void mostrarTabla() {

		equipos.clear();

		try {
			equipos = (List<Equipo>) bd.listar(Equipo.class);
		} catch (Exception e1) {
			Utiles.notificaError(this, "Error al listar la tabla Equipo", e1,
					"Se ha producido un erro al listar la tabla 'Equipo'");
			return;
		}

		String[] tituloColumna = { "ID", "Nombre equipo" };

		try {
			tabla.setModel(new DefaultTableModel(equiposAMatriz(equipos), tituloColumna) {

				@Override
				public boolean isCellEditable(int row, int column) { // all cells
					return false;
				}
			});
		} catch (Exception ex) {
			Utiles.notificaError(this, "Error al listar la tabla Artículos", ex,
					"Se ha producido un erro al listar la tabla 'Artículos'");
			return;
		}

		tabla.grabFocus();
		if (tabla.getRowCount() != 0)
			tabla.setRowSelectionInterval(0, 0);
		tabla.setSelectionMode(0);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.getColumnModel().getColumn(0).setPreferredWidth(35);
		tabla.getColumnModel().getColumn(1).setMinWidth(453);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

	}

	private String[][] equiposAMatriz(List<Equipo> equipos2) {
		String[][] resultado = new String[equipos2.size()][2];

		for (int i = 0; i < equipos2.size(); i++) {
			resultado[i][0] = String.valueOf(equipos2.get(i).getId());
			resultado[i][1] = String.valueOf(equipos2.get(i).getNombre());
		}

		return resultado;
	}

	private void botonAceptar() {

		if (chckbxEliminar.isSelected()) {

			borrar();

		} else {
			if (!nombreEquipo.getText().isEmpty()) {

				if (tabla.getSelectedRow() == -1) {
					guardar();
				} else {
					modificar();
				}

			} else {
				Utiles.notificaError(null, "Nombre vacío", null, "Introduzca un nombre para dar de alta");
			}

		}

		botonCancelar();

	}

	private void modificar() {

		Equipo equipoSeleccionado = equipos.get(tabla.getSelectedRow());

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de modificar el equipo: '" + equipoSeleccionado.getNombre() + "'?", "Modificar equipo",
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				equipoSeleccionado.setNombre(nombreEquipo.getText());
				bd.modificarDatosByID(equipoSeleccionado);
				mostrarTabla();
			} catch (Exception e) {
				Utiles.notificaError(null, "Error al modificar", e, "Se ha producido un error al modificar");
			}
		}

	}

	private void guardar() {
		try {
			Equipo nuevoEquipo = new Equipo(nombreEquipo.getText());

			if (equipos.contains(nuevoEquipo)) {
				Utiles.notificaError(null, "Equipo ya existe", null, "El equipo ya existe");
				return;
			}

			bd.guardarDatos(nuevoEquipo);
			equipos.add(nuevoEquipo);
			mostrarTabla();
			tabla.revalidate();
			tabla.repaint();

		} catch (Exception e1) {
			Utiles.notificaError(null, "Error al guardar", e1,
					"Se ha producido un error al guardar el equipo en la base de datos");
		}
	}

	private void borrar() {
		if (tabla.getSelectedRow() == -1) {
			Utiles.notificaError(null, "Error al eliminar", null, "Para eliminar seleccione un equipo en la tabla");
			return;
		}

		Equipo equipoSeleccionado = equipos.get(tabla.getSelectedRow());

		try {
			@SuppressWarnings("unchecked")
			ArrayList<Jugador> jugadores = (ArrayList<Jugador>) bd.consultarDatosByObjetos(Jugador.class, true,
					equipoSeleccionado);

			if (!jugadores.isEmpty()) {
				Utiles.notificaError(null, "No se puede borrar", null,
						"Hay jugadores en el equipo que desea borrar, elimine los jugadores asociados");
				return;
			}

		} catch (Exception e) {
			Utiles.notificaError(null, "Error al borrar", e,
					"Se ha producido un error al comprobar el quipo en la base de datos");
			return;
		}

		// Se pide confirmacion
		if (JOptionPane.showConfirmDialog(null,
				"¿Estás seguro de eliminar el equipo: '" + equipoSeleccionado.getNombre() + "'?", "Eliminar Equipo",
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			try {
				bd.borrarDatos(equipos.get(tabla.getSelectedRow()));
				mostrarTabla();
			} catch (Exception e1) {
				Utiles.notificaError(null, "Error al borrar", e1,
						"Se ha producido un error al borrar el equipo en la base de datos");
			}
		}

	}
}
