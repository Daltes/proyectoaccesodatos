package vista;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utiles {
	/**
	 * Lanza un mensaje de pop up con un Warning
	 *
	 * @param padre
	 *            JFrame en el que se centrará la ventana
	 * @param titulo
	 *            String que contiene el titulo de la ventana
	 * @param exception
	 *            Exception que ha provocado el error
	 * @param mensaje
	 *            Mensaje que se quiere dar en la ventana
	 */
	public static void notificaError(JFrame padre, String titulo, Exception exception, String mensaje) {
		// Se crea el String que contendrá el mensaje final del error
		String contenido = "";
		// Se comprueba si se ha introducido una excepcion
		if (exception != null) {
			// Se añade el nombre de la excepcion y el mensaje que le acompaña
			contenido += exception.getClass().getName() + "\n" + exception.getMessage() + "\n";
		}
		// Se comprueba si se ha introducido un mensaje
		if (mensaje != null) {
			// Se añade el mensaje al final
			contenido += mensaje;
		}
		// Se muestra el pop up con el mensaje final y el icono de WARNIGN_MESSAGE
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Pasa un Date a string
	 * 
	 * @param fecha
	 *            Date que se pasará a String
	 * @return String con el Date formateado
	 */
	public static String formatearDateToString(Date fecha) {

		return convertirDateToString(fecha, false);

	}

	/**
	 * Pasa un string a Date
	 * 
	 * @param fecha
	 *            String que se desea pasar a Date
	 * @return Date con el string parseado
	 * @throws ParseException
	 */
	public static Date formatearStringToDate(String fecha) throws ParseException {
		return formatearStringToDate(fecha, false);
	}

	public static Date formatearStringToDate(String fecha, boolean tieneHora) throws ParseException {

		if (!tieneHora && !fecha.matches(".{4}[-].{2}[-].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		if (tieneHora && !fecha.matches(".{4}[-].{2}[-].{2}[ ].{2}[:].{2}[:].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		SimpleDateFormat sdf;

		if (tieneHora)
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		else
			sdf = new SimpleDateFormat("yyyy-MM-dd");

		sdf.setLenient(false);
		return sdf.parse(fecha);
	}

	public static String convertirDateToString(java.util.Date fecha, boolean tieneHora) {
		SimpleDateFormat sdf;

		if (tieneHora)
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		else
			sdf = new SimpleDateFormat("yyyy-MM-dd");

		sdf.setLenient(false);

		String resultado = sdf.format(fecha).trim();

		if (!tieneHora && !resultado.matches(".{4}[-].{2}[-].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		if (tieneHora && !resultado.matches(".{4}[-].{2}[-].{2}[ ].{2}[:].{2}[:].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		return resultado;
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Entry<K, V>> list = new ArrayList<>(map.entrySet());
		// System.out.println(list);
		list.sort(Entry.comparingByValue());
		// System.out.println(list);
		Map<K, V> result = new LinkedHashMap<>();
		for (Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		// System.out.println(result);
		return result;
	}
}
