package controladesConexion;

import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;

import interfacesImplementacion.ActualizarIDClaseContenedoras;
import interfacesImplementacion.Persistencia;

public class PersistenciaJDBC implements Persistencia {

	private Connection con;
	private DatabaseMetaData metaDatos;
	private String baseDatos;

	@Override
	public void conectar(FileReader fichero) throws Exception {
		Properties prop = new Properties();
		prop.load(fichero);
		String tipoPer = prop.getProperty("tipoPersistencia");
		if (tipoPer.equals("mysqlJDBC")) {
			String ip = prop.getProperty("mysqlJDBC.servidor");
			String usu = prop.getProperty("mysqlJDBC.usuario");
			String pass = prop.getProperty("mysqlJDBC.password");
			String baseDatos = prop.getProperty("mysqlJDBC.basedatos");

			conectarDB(ip, usu, pass, baseDatos);
		}

	}

	public void conectarDB(String ip, String usu, String pass, String baseDatos) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://" + ip + "/?serverTimezone=UTC", usu, pass);

		metaDatos = con.getMetaData();
		this.baseDatos = baseDatos;
	}

	public void deconectarDB() throws Exception {
		con.close();
	}

	public ArrayList<String> listarTablas(String base) throws Exception {

		ArrayList<String> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getTables(base, null, "%", null);

		while (bbdd.next()) {
			resultado.add(bbdd.getString("TABLE_NAME"));
		}

		return resultado;
	}

	public ArrayList<Columna> listarFKs(String base, String tabla) throws Exception {
		ResultSet rs = metaDatos.getImportedKeys(base, null, tabla);

		ArrayList<Columna> resultado = new ArrayList<>();

		while (rs.next()) {
			String nombreTabla = rs.getString("PKTABLE_NAME");
			resultado.add(new Columna(rs.getString("FKCOLUMN_NAME"), true, nombreTabla));
		}
		/*
		 * ResultSet rs1 = metaDatos.getImportedKeys(baseDatos, null, tabla); while
		 * (rs1.next()) { System.out.println(rs1.getString(1));
		 * System.out.println(rs1.getString(2)); System.out.println(rs1.getString(3));
		 * System.out.println(rs1.getString(4)); System.out.println(rs1.getString(5));
		 * System.out.println(rs1.getString(6));
		 * 
		 * System.out.println(rs1.getString(7)); System.out.println(rs1.getString(8));
		 * System.out.println(rs1.getString(9)); System.out.println(rs1.getString(10));
		 * System.out.println(rs1.getString(11)); System.out.println(rs1.getString(12));
		 * System.out.println(rs1.getString(13)); System.out.println(rs1.getString(14));
		 * 
		 * System.out.println(
		 * "================================================================================="
		 * ); }
		 * 
		 */

		return resultado;
	}

	public ArrayList<Columna> listarColumnas(String base, String tabla) throws Exception {

		ArrayList<Columna> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getColumns(base, null, tabla, "%");

		ArrayList<Columna> fk = listarFKs(base, tabla);

		// System.out.println(fk.toString());
		int cnt = 0;
		while (bbdd.next()) {
			// System.out.println(bbdd.getString("COLUMN_NAME") + "==>" +
			// fk.get(0).getNombre());

			resultado.add(new Columna(bbdd.getString("COLUMN_NAME"), bbdd.getString("TYPE_NAME"), false, "", false,
					bbdd.getString("IS_AUTOINCREMENT").equals("YES")));

			if (!fk.isEmpty() && fk.contains(resultado.get(cnt))) {
				resultado.get(cnt).setEsFK(true);

				boolean encontrado = true;

				for (int i = 0; encontrado && i < fk.size(); i++) {
					if (fk.get(i).equals(resultado.get(cnt))) {
						resultado.get(cnt).setFkNombreTabla(fk.get(i).getFkNombreTabla());
						fk.remove(i);
						encontrado = false;
					}
				}

			}

			cnt++;
			/*
			 * if (!fk.isEmpty() &&
			 * bbdd.getString("COLUMN_NAME").equals(fk.get(0).getNombre())) {
			 * resultado.add(new Columna(bbdd.getString("COLUMN_NAME"),
			 * bbdd.getString("TYPE_NAME"), true, fk.get(0).getFkNombreTabla(), true,
			 * bbdd.getString("IS_AUTOINCREMENT").equals("YES"))); fk.remove(0); } else {
			 * resultado.add(new Columna(bbdd.getString("COLUMN_NAME"),
			 * bbdd.getString("TYPE_NAME"), false, "", true,
			 * bbdd.getString("IS_AUTOINCREMENT").equals("YES"))); }
			 */
		}

		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);
		// System.out.println(pks);
		for (Columna columna : pks) {
			resultado.get(columna.getPosicion() - 1).setEsPK(true);
		}

		return resultado;
	}

	/**
	 * Metodo que guarda un objeto en la base de datos
	 * 
	 * @param objetoGuardar
	 *            Object objeto que se desea guardar
	 * @return boolean TRUE si ha guardado en la base de datos. FALSE si no ha
	 *         podido guardar los datos
	 * @throws Exception
	 */
	public boolean guardarDatos(Object objetoGuardar) throws Exception {
		// Se obtiene la clase
		Class<?> c = objetoGuardar.getClass();
		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = c.getSimpleName().toLowerCase();

		// Se obtienen los nombres de las columnas
		ArrayList<Columna> nombresColumnas = listarColumnas(baseDatos, tabla);

		/*
		 * ============ Se comprueba que no exista en la bbdd ============>>>>
		 */
		ArrayList<String> ids = new ArrayList<>();
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);
		for (Columna columna : pks) {
			Field f = c.getDeclaredField(columna.getNombre());
			f.setAccessible(true);
			ids.add(String.valueOf(f.get(objetoGuardar)));
		}
		if (consultarDatoByID(c, ids.toArray(new String[0])) != null) {
			return false;
		}
		/*
		 * <<<<============ Se comprueba que no exista en la bbdd ============
		 */

		/*
		 * ====================== Se monta la sentencia sql ======================>>>>>
		 */
		String resultadoQuery = "INSERT INTO " + baseDatos + "." + tabla;
		String parametrosQuery = "VALUES( ";
		String nombreColumnasQuery = " (";
		for (int i = 0; i < nombresColumnas.size(); i++) {
			if (!nombresColumnas.get(i).isEsAutoIncremental()) {
				nombreColumnasQuery += nombresColumnas.get(i).getNombre() + ",";
				parametrosQuery += "?,";
			}
		}
		nombreColumnasQuery = nombreColumnasQuery.substring(0, nombreColumnasQuery.length() - 1);
		nombreColumnasQuery += " ) ";

		parametrosQuery = parametrosQuery.substring(0, parametrosQuery.length() - 1);
		parametrosQuery += " )";

		resultadoQuery += nombreColumnasQuery + parametrosQuery;
		/*
		 * <<<<<================ Fin de montar la sentencia sql ================
		 */

		PreparedStatement query = con.prepareStatement(resultadoQuery);

		/*
		 * ================ Se asignan los parametros a la query ================>>>>>
		 */
		int cnt = 1;
		for (Columna columna : nombresColumnas) {

			String nombreColumna = columna.getNombre();

			if (!columna.isEsAutoIncremental()) {
				// Se obtiene el valor del atributo con el nombre de columna de la tabla
				Field f = c.getDeclaredField(nombreColumna);
				f.setAccessible(true);

				switch (columna.getTipo()) {

				case "DATETIME":
					// query.setDate(cnt, new Date(((java.util.Date)
					// f.get(objetoGuardar)).getTime()));
					query.setString(cnt, convertirDateToString((java.util.Date) f.get(objetoGuardar), true));
					break;

				case "DATE":
					// query.setDate(cnt, new Date(((java.util.Date)
					// f.get(objetoGuardar)).getTime()));
					query.setString(cnt, convertirDateToString((java.util.Date) f.get(objetoGuardar), false));
					break;
				case "VARCHAR":
					query.setString(cnt, (String) f.get(objetoGuardar));
					break;
				case "INT":
					query.setInt(cnt, (Integer) f.get(objetoGuardar));
					break;

				default:
					break;
				}
				cnt++;
				// resultado += nombreColumna + "==>" + f.get(objetoGuardar) + "||||";
				// System.out.println(resultado);
				// f.get(objetoGuardar).getClass();
				// columna.getTipo() + ", \n";
			}

			/*
			 * <<<<<============== Fin de asignacion de parametros a la query ==============
			 */
		}

		query.executeUpdate();
		return true;
	}

	/**
	 * Busca las primary keys de una tabla. Los objetos Columna tienen inicializados
	 * el nombre de columna y la posicion que ocupa en la tabla
	 * 
	 * @param baseDatos
	 *            String nombre de la base de datos
	 * @param tabla
	 *            String nombre de la tabla
	 * @return ArrayList<Columna> arrayList con elementos columnas los cuales son la
	 *         pk's.
	 * @throws Exception
	 */
	public ArrayList<Columna> listarPKs(String baseDatos, String tabla) throws Exception {
		ArrayList<Columna> columnas = new ArrayList<>();

		ResultSet rs = metaDatos.getPrimaryKeys(baseDatos, null, tabla);

		while (rs.next()) {
			columnas.add(new Columna(rs.getString("COLUMN_NAME"), rs.getInt("KEY_SEQ")));
		}

		rs.close();

		return columnas;
	}

	/**
	 * Metodo que busca por id en la base de datos y lo transforma en un objeto
	 * 
	 * @param clase
	 *            Class nombre de la clase, el nombre debe coincidir con la tabla en
	 *            la base de datos
	 * @param id
	 *            String valor de la pk en la base de datos
	 * @return Object objeto con los valores de la base de datos, si no se encuentra
	 *         una coincidencia devueve NULL
	 * @throws Exception
	 */
	public Object consultarDatoByID(Class<?> clase, String... ids) throws Exception {

		// Se comprueba que se ha implementado la interfaz
		if (!ActualizarIDClaseContenedoras.class.isAssignableFrom(clase)) {
			throw new IllegalArgumentException(
					"La clase no ha implementado la interfaz: ActualizarIDClaseContenedoras");
		}

		// Se recoge el nombre de la tabla apartir del nombre de la clase
		String tabla = clase.getSimpleName().toLowerCase();

		// Se recoge una lista de las Primary Keys
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);

		// ========== Se monta la clausula WHERE con todas las pks ==========>>>>
		if (ids.length != pks.size()) {
			return null;
		}

		String where = " WHERE ";

		for (int i = 0; i < ids.length; i++) {
			String nombre = pks.get(i).getNombre();
			where += nombre + " = '" + ids[i] + "' AND ";
		}
		where = where.substring(0, where.length() - 5);
		// <<<<========== Se monta la clausula WHERE con todas las pks ==========

		// Se monta la query para recoger los datos
		String busqueda = "SELECT * FROM " + baseDatos + "." + tabla + where;
		// System.out.println(busqueda);
		ResultSet rs = con.createStatement().executeQuery(busqueda);

		// Se crea un objeto Object apartir de la clase que se ha pasado por parametro
		Object objeto = clase.newInstance();

		if (rs.next()) {
			// Se recogen las columnas de la tabla
			ArrayList<Columna> nombresColumnas = listarColumnas(baseDatos, tabla);
			// System.out.println(nombresColumnas);
			for (Columna columna : nombresColumnas) {

				// Se comprueba si es una FK
				if (columna.isEsFK()) {
					// Si es una FK se recoge el nombre de la tabla a la que apunta
					String nombreFKTabla = columna.getFkNombreTabla();
					Field atributo = null;
					try {
						// Se busca el atributo con el nombre de la tabla
						atributo = clase.getDeclaredField(nombreFKTabla);
					} catch (NoSuchFieldException ex) {
						// Si no se encuentra el atributo se prueba con el segundo metodo de busqueda
						// NombreTablaFK + By + NombreColumna
						// El nombreColumna se pone la primera letra en mayusculas
						String nombreColumnaTemp = columna.getNombre().substring(0, 1).toUpperCase()
								+ columna.getNombre().substring(1);

						atributo = clase.getDeclaredField(nombreFKTabla + "By" + nombreColumnaTemp);
					}
					atributo.setAccessible(true);
					// Se monta el nombre completo de la clase usando el nomrbe de la tabla y el
					// paquete
					nombreFKTabla = "clasesContenedoras." + nombreFKTabla.substring(0, 1).toUpperCase()
							+ nombreFKTabla.substring(1);

					// se vuelve a llamar a este mismo metodo para asignar el valor el objeto.
					atributo.set(objeto,
							consultarDatoByID(Class.forName(nombreFKTabla), rs.getString(columna.getNombre())));
				} else {
					// Caso de que no sea una FK
					String nombre = columna.getNombre();
					Field atributo = clase.getDeclaredField(nombre);
					atributo.setAccessible(true);

					// Se le da un valor dependiendo del tipo que sea
					switch (columna.getTipo()) {

					case "DATETIME":
						atributo.set(objeto, new java.util.Date(rs.getDate(nombre).getTime()));
						break;

					case "DATE":
						atributo.set(objeto, new java.util.Date(rs.getDate(nombre).getTime()));
						break;
					case "VARCHAR":
						atributo.set(objeto, rs.getString(nombre));
						break;
					case "INT":
						atributo.set(objeto, rs.getInt(nombre));
						break;

					default:
						break;
					}

				}
			}
		} else {
			// Si no encuentra nada devuelve null
			return null;
		}

		// Se llama al metodo de la interfaz para actualizar los ID's
		Method method = objeto.getClass().getMethod("actualizarID");
		method.invoke(objeto);
		rs.close();

		return objeto;
	}

	/**
	 * Actualiza los datos en la base de datos apartir del objeto que se le pase
	 * 
	 * @param objeto
	 *            Object objeto del cual se sacaran los datos
	 * 
	 * @return boolean TRUE si ha conseguido actualizar. FALSE si no ha podido
	 *         actualizar
	 * 
	 * @throws Exception
	 */
	public boolean modificarDatosByID(Object objeto) throws Exception {

		// Se obtiene la clase
		Class<?> c = objeto.getClass();
		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = c.getSimpleName().toLowerCase();

		/*
		 * ============ Se comprueba que no exista en la bbdd ============>>>>
		 */
		ArrayList<String> idsArrayList = new ArrayList<>();
		// Se recoge una lista de las Primary Keys
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);
		for (Columna columna : pks) {
			Field f = c.getDeclaredField(columna.getNombre());
			f.setAccessible(true);
			idsArrayList.add(String.valueOf(f.get(objeto)));
		}

		// Se obtienen los datos de los ID's
		String[] ids = idsArrayList.toArray(new String[0]);
		// System.out.println(idsArrayList);
		if (consultarDatoByID(c, ids) == null) {
			// System.out.println("esto esta muriendooooo");
			return false;
		}
		/*
		 * <<<<============ Se comprueba que no exista en la bbdd ============
		 */

		// ========== Se monta la clausula WHERE con todas las pks ==========>>>>
		if (ids.length != pks.size()) {
			return false;
		}

		String where = " WHERE ";

		for (int i = 0; i < ids.length; i++) {
			String nombre = pks.get(i).getNombre();
			where += nombre + " = " + ids[i] + " AND ";
		}
		where = where.substring(0, where.length() - 5);
		// <<<<========== Se monta la clausula WHERE con todas las pks ==========

		// Se obtienen los nombres de las columnas
		ArrayList<Columna> nombresColumnas = listarColumnas(baseDatos, tabla);

		/*
		 * ====================== Se monta la sentencia sql ======================>>>>>
		 */
		String resultadoQuery = "UPDATE " + baseDatos + "." + tabla;
		String parametrosQuery = " SET ";
		// System.out.println(nombresColumnas);
		for (int i = 0; i < nombresColumnas.size(); i++) {
			if (!nombresColumnas.get(i).isEsPK()) {
				parametrosQuery += nombresColumnas.get(i).getNombre() + " = ?, ";
			}
		}

		parametrosQuery = parametrosQuery.substring(0, parametrosQuery.length() - 2);
		parametrosQuery += where;
		resultadoQuery += parametrosQuery;
		// System.out.println(resultadoQuery);
		/*
		 * <<<<<================ Fin de montar la sentencia sql ================
		 */

		PreparedStatement query = con.prepareStatement(resultadoQuery);

		/*
		 * ================ Se asignan los parametros a la query ================>>>>>
		 */
		int cnt = 1;
		for (Columna columna : nombresColumnas) {

			String nombreColumna = columna.getNombre();

			if (!columna.isEsPK()) {
				// Se obtiene el valor del atributo con el nombre de columna de la tabla
				Field f = c.getDeclaredField(nombreColumna);
				f.setAccessible(true);

				switch (columna.getTipo()) {

				case "DATETIME":
					// query.setDate(cnt, new Date(((java.util.Date) f.get(objeto)).getTime()));

					query.setString(cnt, convertirDateToString((java.util.Date) f.get(objeto), true));

					break;

				case "DATE":
					// query.setDate(cnt, new Date(((java.util.Date) f.get(objeto)).getTime()));
					query.setString(cnt, convertirDateToString((java.util.Date) f.get(objeto), false));
					break;
				case "VARCHAR":
					query.setString(cnt, (String) f.get(objeto));
					break;
				case "INT":
					query.setInt(cnt, (Integer) f.get(objeto));
					break;

				default:
					break;
				}
				cnt++;
			}
			/*
			 * <<<<<============== Fin de asignacion de parametros a la query ==============
			 */

		}
		// System.out.println(query.toString());
		query.executeUpdate();
		return true;
	}

	private String convertirDateToString(java.util.Date fecha, boolean tieneHora) {
		SimpleDateFormat sdf;

		if (tieneHora)
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		else
			sdf = new SimpleDateFormat("yyyy-MM-dd ");

		sdf.setLenient(false);

		String resultado = sdf.format(fecha).trim();

		if (!tieneHora && !resultado.matches(".{4}[-].{2}[-].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		if (tieneHora && !resultado.matches(".{4}[-].{2}[-].{2}[ ].{2}[:].{2}[:].{2}")) {
			throw new IllegalArgumentException("La fecha no es correcta");
		}

		return resultado;
	}

	/**
	 * Borra el objeto de la base de datos
	 * 
	 * @param objeto
	 *            Object objeto que contiene los datos
	 * @return boolean TRUE si ha conseguido borrarlo, FALSE si el numero de pk's no
	 *         coinciden
	 * @throws Exception
	 */
	public boolean borrarDatos(Object objeto) throws Exception {
		// Se obtiene la clase
		Class<?> c = objeto.getClass();
		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = c.getSimpleName().toLowerCase();

		/*
		 * ============ Se Obtienen las PK's ============>>>>
		 */
		ArrayList<String> idsArrayList = new ArrayList<>();
		// Se recoge una lista de las Primary Keys
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);
		for (Columna columna : pks) {
			Field f = c.getDeclaredField(columna.getNombre());
			f.setAccessible(true);
			idsArrayList.add(String.valueOf(f.get(objeto)));
		}

		// Se obtienen los datos de los ID's
		String[] ids = idsArrayList.toArray(new String[0]);

		/*
		 * <<<<============ Se Obtienen las PK's ============
		 */

		// ========== Se monta la clausula WHERE con todas las pks ==========>>>>
		if (ids.length != pks.size()) {
			return false;
		}

		String where = " WHERE ";

		for (int i = 0; i < ids.length; i++) {
			String nombre = pks.get(i).getNombre();
			where += nombre + " = " + ids[i] + " AND ";
		}
		where = where.substring(0, where.length() - 5);
		// <<<<========== Se monta la clausula WHERE con todas las pks ==========

		String query = "DELETE FROM " + baseDatos + "." + tabla + where;
		// System.out.println(query);

		Statement s = con.createStatement();
		s.executeUpdate(query);

		return true;
	}

	/**
	 * Lista todos los datos de una tabla
	 * 
	 * @param clase
	 *            Clase de la que se buscaran los datos
	 * @return ArrayList<Object> contiene los objetos con los datos
	 * @throws Exception
	 */
	public ArrayList<?> listar(Class<?> clase) throws Exception {

		ArrayList<Object> resultado = new ArrayList<>();

		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = clase.getSimpleName().toLowerCase();

		/*
		 * ============ Se Obtienen las PK's ============>>>>
		 */
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);
		/*
		 * <<<<============ Se Obtienen las PK's ============
		 */

		String query = "SELECT * FROM " + baseDatos + "." + tabla;

		ResultSet rs = con.createStatement().executeQuery(query);

		while (rs.next()) {

			ArrayList<String> idsArrayList = new ArrayList<>();

			// Se recogen los valores de las PK's
			for (Columna columna : pks) {
				idsArrayList.add(rs.getString(columna.getNombre()));
			}

			// Se pasan a un array de strings
			String[] ids = idsArrayList.toArray(new String[0]);

			// Se realiza una busqueda para que rellene todos los datos
			Object busqueda = consultarDatoByID(clase, ids);
			if (busqueda != null) {
				resultado.add(busqueda);
			}

		}

		return resultado;
	}

	@Override
	public ArrayList<?> consultarDatosByObjetos(Class<?> clase, boolean esConsultaAND, Object... campos)
			throws Exception {

		ArrayList<Object> resultado = new ArrayList<>();

		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = clase.getSimpleName().toLowerCase();

		// Sentencia que se ejecutará
		String sentencia = "SELECT ";

		/*
		 * ============ Se Obtienen las PK's ============>>>>
		 */
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);

		for (int i = 0; i < pks.size(); i++) {
			sentencia += pks.get(i).getNombre() + " ,";
		}
		// Se elimina la coma
		sentencia = sentencia.substring(0, sentencia.length() - 1);

		/*
		 * <<<<============ Se Obtienen las PK's ============
		 */

		/*
		 * >>>>> Clausua WHERE >>>>>
		 */

		sentencia += "FROM " + baseDatos + "." + tabla + " WHERE ";
		boolean esPrimeraVez = true;
		for (int i = 0; i < campos.length; i++) {

			if (esPrimeraVez) {
				esPrimeraVez = false;
			} else {
				if (esConsultaAND) {
					sentencia += " AND ";
				} else {
					sentencia += " OR ";
				}
			}

			@SuppressWarnings("rawtypes")
			Class claseCampos = campos[i].getClass();
			String tablaCampos = claseCampos.getSimpleName().toLowerCase();

			ArrayList<Columna> pksCampos = listarPKs(baseDatos, tablaCampos);

			boolean esPrimeraVezFK = true;
			for (int j = 0; j < pksCampos.size(); j++) {
				// Se comprueba el nombre de la columna de la clase a devolver
				String nombreTemp = "id" + tablaCampos.substring(0, 1).toUpperCase() + tablaCampos.substring(1);
				try {
					clase.getDeclaredField(nombreTemp);
				} catch (NoSuchFieldException e) {
					// Si no existe se busca por el segundo metodo de nombramiento
					nombreTemp = "id" + tablaCampos.substring(0, 1).toUpperCase() + tablaCampos.substring(1, 4);
					try {
						clase.getDeclaredField(nombreTemp);
					} catch (NoSuchFieldException e1) {
						nombreTemp = pksCampos.get(j).getNombre();
						clase.getDeclaredField(nombreTemp);
					}
				}

				// Se obtiene el atributo que representa la PK de la FK
				Field fieldFK = claseCampos.getDeclaredField(pksCampos.get(j).getNombre());
				fieldFK.setAccessible(true);

				if (esPrimeraVezFK) {
					esPrimeraVezFK = false;
				} else {

					sentencia += " AND ";

				}

				sentencia += nombreTemp + "=" + fieldFK.get(campos[i]);
			}

		}
		/*
		 * <<<<< Clausula WHERE <<<<<
		 */

		/*
		 * >>>>> Creacion de la lista >>>>>
		 */

		ResultSet rs = con.createStatement().executeQuery(sentencia);

		while (rs.next()) {
			String[] pksConsultadas = new String[pks.size()];
			for (int j = 0; j < pks.size(); j++) {
				pksConsultadas[j] = rs.getString(j + 1);
			}

			resultado.add(consultarDatoByID(clase, pksConsultadas));
			// resultado.add(consultarDatoByID(clase, rs.getString(1)));
		}

		/*
		 * <<<<< Creacion de la lista <<<<<
		 */

		// System.out.println(sentencia);
		return resultado;
	}

	@Override
	public ArrayList<?> consultarDatosByCampos(Class<?> clase, Campo... campos) throws Exception {

		ArrayList<Object> resultado = new ArrayList<>();

		// Se obtiene el nombre de la tabla con el nombre de la clase
		String tabla = clase.getSimpleName().toLowerCase();

		// Sentencia que se ejecutará
		String sentencia = "SELECT ";

		/*
		 * ============ Se Obtienen las PK's ============>>>>
		 */
		String ids = "";
		ArrayList<Columna> pks = listarPKs(baseDatos, tabla);

		for (int i = 0; i < pks.size(); i++) {
			ids += pks.get(i).getNombre() + " ,";
		}
		// Se elimina la coma
		ids = ids.substring(0, ids.length() - 2);

		/*
		 * <<<<============ Se Obtienen las PK's ============
		 */

		/*
		 * >>>>> Se obtiene el FROM >>>>>
		 */

		String from = " FROM " + baseDatos + "." + clase.getSimpleName().toLowerCase();

		/*
		 * <<<<< Se obtiene el FROM <<<<<
		 */

		/*
		 * >>>>> Se obtiene el WHERE >>>>>
		 */

		String where = " WHERE";
		boolean esPrimeraVez = true;
		for (int i = 0; i < campos.length; i++) {

			if (esPrimeraVez) {
				esPrimeraVez = false;
			} else {
				if (campos[i].isEsAND()) {
					where += " AND";
				} else {
					where += " OR";
				}
			}

			// System.out.println(campos[i].getObjeto().getClass().getName().split("\\.")[0].equals("clasesContenedoras"));
			// System.out.println(where.getClass().getName().split("\\.")[0]);
			String nombreTemp = campos[i].getNombre();
			if (campos[i].getObjeto().getClass().getName().split("\\.")[0].equals("clasesContenedoras")) {
				Class<?> claseCampos = campos[i].getObjeto().getClass();
				String tablaCampos = claseCampos.getSimpleName().toLowerCase();

				ArrayList<Columna> pksCampos = listarPKs(baseDatos, tablaCampos);

				boolean esPrimeraVezFK = true;
				for (int j = 0; j < pksCampos.size(); j++) {
					// Se comprueba el nombre de la columna de la clase a devolver

					try {
						clase.getDeclaredField(campos[i].getNombre());
					} catch (NoSuchFieldException e) {
						// Si no existe se busca por el segundo metodo de nombramiento
						nombreTemp = "id" + tablaCampos.substring(0, 1).toUpperCase() + tablaCampos.substring(1);
						clase.getDeclaredField(nombreTemp);

					}

					// Se obtiene el atributo que representa la PK de la FK
					Field fieldFK = claseCampos.getDeclaredField(pksCampos.get(j).getNombre());
					fieldFK.setAccessible(true);

					if (esPrimeraVezFK) {
						esPrimeraVezFK = false;
					} else {

						sentencia += " AND ";

					}
					where += " " + nombreTemp + "=";
					where += fieldFK.get(campos[i].getObjeto());
				}

			} else {
				where += " " + nombreTemp + "=";
				where += campos[i].getObjeto().toString();
			}

		}

		/*
		 * <<<<< Se obtiene el WHERE <<<<<<
		 */

		/*
		 * >>>>> Creacion de la lista >>>>>
		 */
		sentencia += ids + from + where;
		ResultSet rs = con.createStatement().executeQuery(sentencia);

		while (rs.next()) {

			resultado.add(consultarDatoByID(clase, rs.getString(1)));
		}

		/*
		 * <<<<< Creacion de la lista <<<<<
		 */
		// System.out.println(sentencia);
		return resultado;
	}

}
