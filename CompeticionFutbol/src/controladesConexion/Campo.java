package controladesConexion;

public class Campo {
	Object objeto;
	boolean esAND;
	String nombre;

	public Campo(String nombre, Object objeto) {
		super();
		this.objeto = objeto;
		this.nombre = nombre;
	}

	public Campo(String nombre, Object objeto, boolean esAND) {
		super();
		this.objeto = objeto;
		this.esAND = esAND;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public boolean isEsAND() {
		return esAND;
	}

	public void setEsAND(boolean esAND) {
		this.esAND = esAND;
	}

}
