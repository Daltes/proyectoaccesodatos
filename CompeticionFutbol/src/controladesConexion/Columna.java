package controladesConexion;

public class Columna {
	private String nombre;
	private String tipo;
	private int posicion;
	private boolean esFK;
	private String fkNombreTabla;
	private boolean esAutoIncremental;
	private boolean esPK;

	public Columna(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
	}

	public Columna(String nombre, String tipo, int posicion, boolean esFK) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		this.posicion = posicion;
		this.esFK = esFK;
	}

	public Columna(String nombre, int posicion) {
		super();
		this.nombre = nombre;
		this.posicion = posicion;
	}

	public Columna(String nombre, boolean esFK, String fkNombreTabla) {
		super();
		this.nombre = nombre;
		this.esFK = esFK;
		this.fkNombreTabla = fkNombreTabla;
	}
	

	public Columna(String nombre, String tipo, boolean esFK, String fkNombreTabla,boolean esPK, boolean esAutoIncremental) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		this.esFK = esFK;
		this.fkNombreTabla = fkNombreTabla;
		this.esPK = esPK;
		this.esAutoIncremental = esAutoIncremental;
	}
	
	public boolean isEsPK() {
		return esPK;
	}

	public void setEsPK(boolean esPK) {
		this.esPK = esPK;
	}

	public String getFkNombreTabla() {
		return fkNombreTabla;
	}

	public boolean isEsAutoIncremental() {
		return esAutoIncremental;
	}

	public void setEsAutoIncremental(boolean esAutoIncremental) {
		this.esAutoIncremental = esAutoIncremental;
	}

	public void setFkNombreTabla(String fkNombreTabla) {
		this.fkNombreTabla = fkNombreTabla;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public boolean isEsFK() {
		return esFK;
	}

	public void setEsFK(boolean esFK) {
		this.esFK = esFK;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Columna other = (Columna) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Columna [nombre=" + nombre + ", tipo=" + tipo + ", posicion=" + posicion + ", esFK=" + esFK
				+ ", fkNombreTabla=" + fkNombreTabla + ", esAutoIncremental=" + esAutoIncremental + ", esPK=" + esPK
				+ "]";
	}
	
	
	
	
	
	

}
