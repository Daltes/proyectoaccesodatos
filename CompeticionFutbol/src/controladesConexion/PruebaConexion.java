package controladesConexion;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import clasesContenedoras.Competicion;
import clasesContenedoras.Equipo;
import clasesContenedoras.Estadistica;
import clasesContenedoras.Jugador;
import clasesContenedoras.Partido;
import clasesContenedoras.Posicion;

@SuppressWarnings("unused")
public class PruebaConexion {

	public static void main(String[] args) throws Exception {

		PersistenciaJDBC bd = new PersistenciaJDBC();
		bd.conectarDB("localhost", "root", "root", "competicionesFutbol");

		 //PersistenciaHibernate bd = new PersistenciaHibernate();
		 //File f = new File("src/hibernate.cfg.xml");
		// bd.conectar(f);

		Competicion com = new Competicion("NombreCompeticion", new Date(System.currentTimeMillis()),
				new Date(System.currentTimeMillis()));
		Equipo p = new Equipo("NombreEquipo");
		com.setId(1);
		Equipo equipo = (Equipo) bd.consultarDatoByID(Equipo.class, "99999999");
		
		Estadistica e = (Estadistica) bd.consultarDatoByID(Estadistica.class, "2", "1");

		Partido partido = (Partido) bd.consultarDatoByID(Partido.class, "2");

		Posicion posicion = (Posicion) bd.consultarDatoByID(Posicion.class, "1");

		Jugador jugador = (Jugador) bd.consultarDatoByID(Jugador.class, "2");

		System.out.println(equipo);

		partido.setFechaHora(new Date());
		partido.setGolesLocal(5);
		
		bd.modificarDatosByID(partido);
		
		Campo campo = new Campo("golesLocal", new Integer(0));

		System.out.println(bd.consultarDatosByCampos(Partido.class, campo).size());
		// bd.guardarDatos(partido);

		// com = (Competicion) bd.consultarDatoByID(Competicion.class, "2");

		// System.out.println(bd.consultarDatosByCamposObjetos(Partido.class,
		// true,jugador ).size());

		// p.setId(1);
		// Partido part = new Partido(p, p, com, new Date(), 0, 0);

		// Jugador j = new Jugador(1,(Equipo) bd.consultarDatoByID(new
		// Equipo().getClass(), "1"),(Posicion)bd.consultarDatoByID(new
		// Posicion().getClass(), "1") , "Nombre Jugador", 35);

		// j.setLicencia(2); //System.out.println(bd.guardarDatos(j));
		// System.out.println(j);

		// bd.guardarDatos(p);

		// Estadistica e = (Estadistica)
		// bd.consultarDatoByID(Estadistica.class,"2","1");

		// Equipo equipo = (Equipo) bd.consultarDatoByID(Equipo.class, "1");
		// equipo.setNombre("Nombre IDS variables SIN ID'S Modificado");
		// System.out.println(bd.modificarDatosByID(equipo));

		// bd.borrarDatos(e);

		// System.out.println(((Partido)bd.consultarDatoByID(newPartido().getClass(),"2")));

		// Equipo e = (Equipo) bd.consultarDatoByID(Equipo.class, "4");

		// bd.borrarDatos(e);

		// ArrayList<Object>lista = bd.listar(Jugador.class);

		// for (Object object : lista) { System.out.println(object); }

		// Posicion p = new Posicion("prueba");
		// p.setId(1);
		// System.out.println(bd.listar(Posicion.class));
		/*
		 * Equipo e1 = (Equipo) bd.consultarDatoByID(Equipo.class, "2"); Equipo e2 =
		 * (Equipo) bd.consultarDatoByID(Equipo.class, "3");
		 * 
		 * Posicion p = (Posicion) bd.consultarDatoByID(Posicion.class, "1"); Jugador j1
		 * = new Jugador(1, e1, p, "jugador1", 123); Jugador j2 = new Jugador(2, e1, p,
		 * "jugador2", 123); Jugador j3 = new Jugador(3, e1, p, "jugador3", 123);
		 * Jugador j4 = new Jugador(4, e1, p, "jugador4", 123); Jugador j5 = new
		 * Jugador(5, e2, p, "jugador5", 123); Jugador j6 = new Jugador(6, e2, p,
		 * "jugador6", 123); Jugador j7 = new Jugador(7, e2, p, "jugador7", 123);
		 * Jugador j8 = new Jugador(8, e2, p, "jugador8", 123);
		 * 
		 * bd.guardarDatos(e1); bd.guardarDatos(e2);
		 * 
		 * bd.guardarDatos(j1); bd.guardarDatos(j2); bd.guardarDatos(j3);
		 * bd.guardarDatos(j4); bd.guardarDatos(j5); bd.guardarDatos(j6);
		 * bd.guardarDatos(j7); bd.guardarDatos(j8);
		 * 
		 */

		// TreeMap<String, String> campos = new TreeMap<>();

		// campos.put("idEquipo", "2");
		// Equipo e = (Equipo) bd.consultarDatoByID(Equipo.class, "1");
		// Posicion p = (Posicion) bd.consultarDatoByID(Posicion.class, "3");

		// System.out.println(bd.consultarDatosByCampos(Jugador.class, campos,
		// false).size());
		/// System.out.println(bd2.consultarDatosByObjetos(Partido.class,true,
		/// jugador));
	}

}