package controladesConexion;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import clasesContenedoras.Estadistica;
import clasesContenedoras.EstadisticaId;
import interfacesImplementacion.Persistencia;

public class PersistenciaHibernate implements Persistencia {
	private static SessionFactory sessionFactory;
	private Session session;

	@Override
	public void conectar(FileReader fichero) throws Exception {
		Properties prop = new Properties();
		prop.load(fichero);
		String tipoPer = prop.getProperty("tipoPersistencia");

		if (tipoPer.equals("hibernate")) {
			String ruta = prop.getProperty("hibernate.archivoCFG");
			conectar(new File(ruta));
		}

	}

	public void conectar(File archivoCFG) throws Exception {
		if (!archivoCFG.exists()) {
			throw new IllegalArgumentException("El fichero no existe de configuración de hibernate no existe");
		}
		if (!archivoCFG.canRead()) {
			throw new IllegalArgumentException("No se puede leer el fichero de configuración de hibernate");
		}
		Configuration configuration = new Configuration();
		configuration.configure(archivoCFG);
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		session = sessionFactory.openSession();
	}

	/**
	 * Actualiza los datos en la base de datos apartir del objeto que se le pase
	 * 
	 * @param objeto
	 *            Object objeto del cual se sacaran los datos
	 * @param ids
	 *            String varparams con los ids
	 * @return boolean TRUE si ha conseguido actualizar. FALSE si no ha podido
	 *         actualizar
	 * @throws Exception
	 */
	@Override
	public boolean modificarDatosByID(Object objeto) throws Exception {

		guardarDatos(objeto);
		return true;
	}

	/**
	 * Borra el objeto de la base de datos
	 * 
	 * @param objeto
	 *            Object objeto que contiene los datos
	 * @return boolean TRUE si ha conseguido borrarlo, FALSE si el numero de pk's no
	 *         coinciden
	 * @throws Exception
	 */
	@Override
	public boolean borrarDatos(Object objecto) throws Exception {
		session.beginTransaction();
		session.delete(objecto); // <|--- Aqui borramos el objeto en la base de datos.
		if (!session.getTransaction().wasCommitted())
			session.getTransaction().commit();
		return true;
	}

	/**
	 * Metodo que busca por id en la base de datos y lo transforma en un objeto
	 * 
	 * @param clase
	 *            Class nombre de la clase, el nombre debe coincidir con la tabla en
	 *            la base de datos
	 * @param id
	 *            String valor de la pk en la base de datos
	 * @return Object objeto con los valores de la base de datos, si no se encuentra
	 *         una coincidencia devueve NULL
	 * @throws Exception
	 */
	@Override
	public Object consultarDatoByID(Class<?> clase, String... ids) throws Exception {

		if (clase == Estadistica.class) {
			if (ids.length != 2) {
				throw new IllegalArgumentException("Numero de ID's insuficientes");
			}
			EstadisticaId estadisticaId = new EstadisticaId(Integer.parseInt(ids[0]), Integer.parseInt(ids[1]));
			return session.get(clase, estadisticaId);
		}

		return session.get(clase, Integer.parseInt(ids[0]));
	}

	/**
	 * Metodo que guarda un objeto en la base de datos
	 * 
	 * @param objetoGuardar
	 *            Object objeto que se desea guardar
	 * @return boolean TRUE si ha guardado en la base de datos. FALSE si no ha
	 *         podido guardar los datos
	 * @throws Exception
	 */
	@Override
	public boolean guardarDatos(Object objeto) throws Exception {
		session.beginTransaction();
		session.save(objeto);
		if (!session.getTransaction().wasCommitted())
			session.getTransaction().commit();
		return true;
	}

	/**
	 * Lista todos los datos de una tabla
	 * 
	 * @param clase
	 *            Clase de la que se buscaran los datos
	 * @return ArrayList<Object> contiene los objetos con los datos
	 * @throws Exception
	 */
	@Override
	public ArrayList<?> listar(Class<?> clase) throws Exception {

		Query query = session.createQuery("from " + clase.getName());

		return (ArrayList<?>) query.list();
	}

	@Override
	public ArrayList<?> consultarDatosByObjetos(Class<?> clase, boolean esConsultaAND, Object... campos)
			throws Exception {
		String tabla = clase.getSimpleName();

		String sentencia = "SELECT e FROM " + tabla + " e WHERE " + "e.";

		boolean esPrimeraVez = true;
		for (int i = 0; i < campos.length; i++) {

			if (esPrimeraVez) {
				esPrimeraVez = false;
			} else {
				if (esConsultaAND) {
					sentencia += " AND e.";
				} else {
					sentencia += " OR e.";
				}
			}

			String nombre = campos[i].getClass().getSimpleName().split("_")[0].toLowerCase();

			sentencia += nombre + " = :" + campos[i].getClass().getSimpleName().toLowerCase();
		}

		// System.out.println(sentencia);
		Query query = session.createQuery(sentencia);

		for (int i = 0; i < campos.length; i++) {
			query.setParameter(campos[i].getClass().getSimpleName().toLowerCase(), campos[i]);
		}

		return (ArrayList<?>) query.list();
	}

	@Override
	public ArrayList<?> consultarDatosByCampos(Class<?> clase, Campo... campos) throws Exception {

		String tabla = clase.getSimpleName();

		String sentencia = "SELECT e FROM " + tabla + " e WHERE " + "e.";

		String where = "";
		boolean esPrimeraVez = true;
		for (int i = 0; i < campos.length; i++) {

			if (esPrimeraVez) {
				esPrimeraVez = false;
			} else {
				if (campos[i].esAND) {
					where += " AND e.";
				} else {
					where += " OR e.";
				}
			}

			if (campos[i].getObjeto().getClass().getName().split("\\.")[0].equals("clasesContenedoras")) {

				String nombreTemp = campos[i].getNombre();

				try {

					nombreTemp = campos[i].getObjeto().getClass().getSimpleName().toLowerCase() + "By"
							+ nombreTemp.substring(0, 1).toUpperCase() + nombreTemp.substring(1);

					clase.getDeclaredField(nombreTemp);
				} catch (NoSuchFieldException e) {
					// Si no existe se busca por el segundo metodo de nombramiento
					nombreTemp = campos[i].getObjeto().getClass().getSimpleName().toLowerCase();
					clase.getDeclaredField(nombreTemp);

				}

				where += nombreTemp + "=:" + campos[i].getObjeto().getClass().getSimpleName().toLowerCase();

			} else {
				where += campos[i].getNombre() + " = " + campos[i].getObjeto().toString();
				// System.out.println("SOY DE JAVAAAA");
			}

		}

		// System.out.println(sentencia);
		Query query = session.createQuery(sentencia + where);

		for (int i = 0; i < campos.length; i++) {
			if (campos[i].getObjeto().getClass().getName().split("\\.")[0].equals("clasesContenedoras")) {
				query.setParameter(campos[i].getObjeto().getClass().getSimpleName().toLowerCase(),
						campos[i].getObjeto());
			}
		}

		return (ArrayList<?>) query.list();
	}

}
