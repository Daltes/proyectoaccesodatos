package interfacesImplementacion;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;

import controladesConexion.Campo;

public interface Persistencia {

	/**
	 * Establece conexción con la base de datos
	 * 
	 * @param fichero
	 *            FileReader que contiene los datos de configuracion
	 * @throws Exception
	 */
	public void conectar(FileReader fichero) throws Exception;

	/**
	 * Actualiza los datos en la base de datos apartir del objeto que se le pase
	 * 
	 * @param objeto
	 *            Object objeto del cual se sacaran los datos
	 * @param ids
	 *            String varparams con los ids
	 * @return boolean TRUE si ha conseguido actualizar. FALSE si no ha podido
	 *         actualizar
	 * 
	 * @throws Exception
	 */
	public boolean modificarDatosByID(Object objeto) throws Exception;

	/**
	 * Borra el objeto de la base de datos
	 * 
	 * @param objeto
	 *            Object objeto que contiene los datos
	 * @return boolean TRUE si ha conseguido borrarlo, FALSE si el numero de pk's no
	 *         coinciden
	 * 
	 * @throws Exception
	 */
	public boolean borrarDatos(Object objecto) throws Exception;

	/**
	 * Metodo que busca por id en la base de datos y lo transforma en un objeto
	 * 
	 * @param clase
	 *            Class nombre de la clase, el nombre debe coincidir con la tabla en
	 *            la base de datos
	 * @param id
	 *            String valor de la pk en la base de datos
	 * @return Object objeto con los valores de la base de datos, si no se encuentra
	 *         una coincidencia devueve NULL
	 * 
	 * @throws Exception
	 */
	public Object consultarDatoByID(Class<?> clase, String... ids) throws Exception;

	/**
	 * Metodo que guarda un objeto en la base de datos
	 * 
	 * @param objetoGuardar
	 *            Object objeto que se desea guardar
	 * @return boolean TRUE si ha guardado en la base de datos. FALSE si no ha
	 *         podido guardar los datos
	 * 
	 * @throws Exception
	 */
	public boolean guardarDatos(Object objeto) throws Exception;

	/**
	 * Lista todos los datos de una tabla
	 * 
	 * @param clase
	 *            Clase de la que se buscaran los datos
	 * @return ArrayList<Object> contiene los objetos con los datos
	 * 
	 * @throws Exception
	 */
	public ArrayList<?> listar(Class<?> clase) throws Exception;

	/**
	 * Lista todos los datos que coincidan con los objetos indicados. El nombre del
	 * campo lo obtiene del objeto, si este no coincide, usar el metodo
	 * "consultarDatosByCampos()" para mayor presicion
	 * 
	 * @param clase
	 *            Class del que se buscará en la base de datos.
	 * @param campos
	 *            Object[] con las condiciones que tendra la query.
	 * @param esConsultaAND
	 *            boolean que indica si será una busqueda con AND u OR. TRUE usará
	 *            AND, FALSE usará OR.
	 * @return ArrayList<Object> que contendrá el resultado de la búsqueda.
	 * 
	 * @throws Exception
	 */
	public ArrayList<?> consultarDatosByObjetos(Class<?> clase, boolean esConsultaAND, Object... campos)
			throws Exception;

	/**
	 * Realiza una busqueda indicando la columna con su valor, y el objeto a
	 * devolver
	 * 
	 * @param clase
	 *            Class Clase del objeto que se desea devolver
	 * @param campos
	 *            Campos que contiene el nombre del campo, su valor y si se usará
	 *            AND u OR
	 * @return ArrayList con los objetos encontrados con la query
	 * @throws Exception
	 */
	public ArrayList<?> consultarDatosByCampos(Class<?> clase, Campo... campos) throws Exception;
}
