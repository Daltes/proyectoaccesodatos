-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: competicionesFutbol
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `competicion`
--

DROP TABLE IF EXISTS `competicion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `fechaComienzo` date NOT NULL,
  `fechaFin` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competicion`
--

LOCK TABLES `competicion` WRITE;
/*!40000 ALTER TABLE `competicion` DISABLE KEYS */;
INSERT INTO `competicion` VALUES (1,'Champions League','2018-12-31','2019-01-31'),(2,'Copa del Rey','2019-12-31','2020-01-31');
/*!40000 ALTER TABLE `competicion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (1,'Barcelona'),(2,'Real Madrid'),(3,'Atlético de Madrid'),(4,'Betis');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadistica`
--

DROP TABLE IF EXISTS `estadistica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadistica` (
  `idPartido` int(11) NOT NULL,
  `licencia` int(11) NOT NULL,
  `goles` int(11) NOT NULL,
  `faltas` int(11) NOT NULL,
  `tarjAmarillas` int(11) NOT NULL,
  `tarjRojas` int(11) NOT NULL,
  PRIMARY KEY (`idPartido`,`licencia`),
  KEY `fk_estadistica_partido1_idx` (`idPartido`),
  KEY `fk_estadistica_jugador1_idx` (`licencia`),
  CONSTRAINT `fk_estadistica_jugador1` FOREIGN KEY (`licencia`) REFERENCES `jugador` (`licencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_estadistica_partido1` FOREIGN KEY (`idPartido`) REFERENCES `partido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadistica`
--

LOCK TABLES `estadistica` WRITE;
/*!40000 ALTER TABLE `estadistica` DISABLE KEYS */;
INSERT INTO `estadistica` VALUES (13,14,1,3,1,0),(15,10,2,0,0,0),(15,12,1,3,2,1),(15,15,1,0,0,0),(16,1,2,1,1,0),(18,5,3,2,1,0);
/*!40000 ALTER TABLE `estadistica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jugador`
--

DROP TABLE IF EXISTS `jugador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugador` (
  `licencia` int(11) NOT NULL,
  `idEquipo` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `dorsal` int(11) NOT NULL,
  `idPosicion` int(11) NOT NULL,
  PRIMARY KEY (`licencia`),
  KEY `fk_jugador_equipo_idx` (`idEquipo`),
  KEY `fk_jugador_posicion1_idx` (`idPosicion`),
  CONSTRAINT `fk_jugador_equipo` FOREIGN KEY (`idEquipo`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jugador_posicion1` FOREIGN KEY (`idPosicion`) REFERENCES `posicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugador`
--

LOCK TABLES `jugador` WRITE;
/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` VALUES (1,1,'Portero Barcelona',1,1),(2,1,'Defensa Barcelona 1',2,2),(3,1,'Defensa Barcelona 2',3,2),(4,1,'Delantero Barcelona 1',4,3),(5,1,'Delantero Barcelona 2',5,3),(6,2,'Portero Madrid',1,1),(7,2,'Defensa Madrid 1',2,2),(8,2,'Defensa Madrid 2',3,2),(9,2,'Delantero Madrid 1',4,3),(10,2,'Delantero Madrid 2',5,3),(11,3,'Portero Atlético',1,1),(12,3,'Defensa Atlético 1',2,2),(13,3,'Defensa Atlético 2',3,2),(14,3,'Delantero Atlético 1',4,3),(15,3,'Delantero Atlético 2',5,3),(16,4,'Portero Betis',1,1),(17,4,'Defensa Betis 1',2,2),(18,4,'Defensa Betis 2',3,2),(19,4,'Delantero Betis 1',4,3),(20,4,'Delantero Betis 2',5,3);
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partido`
--

DROP TABLE IF EXISTS `partido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaHora` datetime NOT NULL,
  `idLocal` int(11) NOT NULL,
  `idVisitante` int(11) NOT NULL,
  `idComp` int(11) NOT NULL,
  `golesLocal` int(11) NOT NULL,
  `golesVisitante` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_partido_equipo1_idx` (`idLocal`),
  KEY `fk_partido_equipo2_idx` (`idVisitante`),
  KEY `fk_partido_competicion1_idx` (`idComp`),
  CONSTRAINT `fk_partido_competicion1` FOREIGN KEY (`idComp`) REFERENCES `competicion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partido_equipo1` FOREIGN KEY (`idLocal`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partido_equipo2` FOREIGN KEY (`idVisitante`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partido`
--

LOCK TABLES `partido` WRITE;
/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
INSERT INTO `partido` VALUES (7,'2019-12-31 00:00:00',3,4,2,0,0),(8,'2020-01-10 00:00:00',1,3,2,0,0),(9,'2020-01-20 00:00:00',3,2,2,0,0),(10,'2020-01-30 00:00:00',4,1,2,0,0),(11,'2020-02-09 00:00:00',4,2,2,0,0),(12,'2020-02-19 00:00:00',1,2,2,0,0),(13,'2018-12-31 00:00:00',3,4,1,1,0),(14,'2019-01-02 00:00:00',1,3,1,0,0),(15,'2019-01-04 00:00:00',3,2,1,2,2),(16,'2019-01-06 00:00:00',4,1,1,0,2),(17,'2019-01-08 00:00:00',4,2,1,0,0),(18,'2019-01-10 00:00:00',1,2,1,3,0);
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posicion`
--

DROP TABLE IF EXISTS `posicion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posicion`
--

LOCK TABLES `posicion` WRITE;
/*!40000 ALTER TABLE `posicion` DISABLE KEYS */;
INSERT INTO `posicion` VALUES (1,'Portero'),(2,'Defensa'),(3,'Delantero');
/*!40000 ALTER TABLE `posicion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-24 18:15:32
